#!/bin/bash

# Get the base path of the script
BASEPATH=$(dirname "$0")

shopt -s nullglob

# Function to echo a message to STDERR
stderr_echo () {
    local msg="$1"
    >&2 echo "$msg"
}

if [ "$1" == "" ]; then
    echo "Usage: $0 <fileglob>"
    exit 1
fi

# Check for the presence of `aspell` (required)
ASPELL=$(command -v aspell)
res=$?
if [ "$res" -ne "0" ]; then
    stderr_echo "It does not appear that 'aspell' is installed (or in the current PATH); please"
    stderr_echo "install before running this script."
    exit 1
fi

# Check for the presence of `pandoc` (optional)
# If `pandoc` is installed, it is used to convert the specified files into HTML as `aspell` is
# more familiar with that format
PANDOC_PRESENT=no
PANDOC=$(command -v pandoc)
res=$?
if [ "$res" -ne "0" ]; then
    stderr_echo "It does not appear that 'pandoc' is installed (or in the current PATH); ignoring."
    stderr_echo "Output results may contain markup words and characters."
else
    stderr_echo "Using Pandoc"
    PANDOC_PRESENT=yes
fi

# If no custom dictionaries are specified, then populate the list with the dictionary files
# that reside in the directory with this script
declare -a CUSTOM_DICTIONARY_LIST
if [ -z "$CUSTOM_DICTIONARIES" ]; then
    CUSTOM_DICTIONARY_LIST=("$BASEPATH"/*.pws )
else
    while read -r dictionary; do
        CUSTOM_DICTIONARY_LIST+=( "$dictionary" )
    done < <( echo "$CUSTOM_DICTIONARIES" )
fi

# Assemble the dictionary files into an option string for `aspell`
personalOption=
for dictionary in "${CUSTOM_DICTIONARY_LIST[@]}"; do
    stderr_echo "*** Using custom dictionary '$dictionary'"
    dictionary_fullpath=$(realpath "$dictionary")
    personalOption+="--add-extra-dicts $dictionary_fullpath "
done

# A list of spelling mistakes
spellingMistakes=()

# Loop through the specified fileglob and gather the list of misspelled words
while (( "$#" )); do
    filename="$1"

    while read -r word; do
        while read -r match; do
            key="$filename:$match"
            spellingMistakes+=("$key")
        done < <( grep -on "\<$word\>" "$filename" )
    done < <( ([ "$PANDOC_PRESENT" == "yes" ] && $PANDOC -f markdown -t html "$filename" || cat "$filename") | $ASPELL pipe list -d en_US -H --ignore 2 $personalOption |

    # Process the aspell output...
    grep '[a-zA-Z]\+ [0-9]\+ [0-9]\+' -oh | \
    grep '[a-zA-Z]\+' -o | \
    sort -d -b | \
    uniq -u )

    shift
done

# Display the list of misspelled words (sorted by filename)
sortedSpellingMistakes=( $( echo "${spellingMistakes[*]}" | sort -b ) )
for i in "${sortedSpellingMistakes[@]}"; do
    IFS=':' read -r -a array <<< "$i"
    printf "%s:%s %s\n" "${array[0]}" "${array[1]}" "${array[2]}"
done


