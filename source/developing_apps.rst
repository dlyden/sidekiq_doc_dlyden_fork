.. include:: replacements.rst

.. _section_sw_development_flow:

*************************
Software Development Flow
*************************

The software development flow for a Sidekiq system is similar to
developing software applications for any Linux-based system. The
general flow is described below.

.. note:: This development flow assumes that a user is developing software on
   the laptop delivered with the Sidekiq PDK

-  Develop the source code for the custom software application on a PC
   such as the Sidekiq PDK laptop, using a text editor of choice
   (:code:`emacs`, :code:`vi`, :code:`gedit`, etc.)
-  Compile and link the source code that makes up the custom software
   application using the GCC toolchain. This includes linking the custom
   application against the appropriate |libsidekiq| userspace library that
   provides the API to access the RF transceiver hardware. For example,
   if a user is developing an application to execute on the Sidekiq PDK
   laptop, which has an Intel x86 CPU and a 64-bit Linux OS installed,
   then the ``libsidekiq__x86_64.gcc.a`` userspace library would be linked
   against. This process will output a binary executable file that will
   run on Linux on the Sidekiq PDK laptop. For details on compiling
   for alternative host platforms, refer to :ref:`section_alternative_host`.
   For details on developing for Windows, refer to :ref:`windows`.
-  Execute the custom application on the Sidekiq system.


********************************************************
Tools/Libraries Needed for Linux Application Development
********************************************************

The following section provides an outline of all prerequisites needed to
develop Linux-based software applications that execute on Sidekiq PDK
laptop. For details on building on a Windows-based system, refer to
:ref:`windows`.


GCC Toolchain
=============

The GNU Compiler Collection (GCC) toolchain :ref:`[3] <ref3>` provides
a robust and widely used set of tools that can be used for building
software applications for a variety of different target platforms. The
GCC toolchain includes many different software components, including the
appropriate C/C++ pre-processor, C/C++ compiler, linker, and other
components needed as part of the building process.

The Sidekiq PDK laptop comes with the appropriate GCC toolchain
pre-installed.

|libsidekiq| Userspace Library
==============================

The |libsidekiq| userspace library provides a high-level API for
configuring the RF transceivers and transferring data between the CPU
and the FPGA. This library is provided as a static library
(e.g. ``libsidekiq__x86_64.gcc.a``), such that a software application would
link against this library as part of the software build process.

The current version of the |libsidekiq| userspace library
can be downloaded at :ref:`[5] <ref5>` as part of the Sidekiq SDK. The
complete documentation for the API provided by this library can be
found as a separate document at :ref:`[5] <ref5>`.

Source Code Editor
==================

A standard text editor is used for editing the C/C++ source code that
make up a Sidekiq software application. Most host PC Linux distributions
ship with a variety of text editors already installed, including
:code:`emacs`, :code:`vi`, :code:`gedit`, and others;  any text editor can be used for
developing software applications for Sidekiq.


Re-Building the Sidekiq Test Applications
=========================================

Each Sidekiq system ships with a suite of test applications installed in
the ``/home/sidekiq/sidekiq_image_current`` directory
(by default). These applications provide a means to demonstrate various
features in the hardware, including receiving I/Q samples from the RF
receivers, (re)programming the FPGA and reading sensors in the system
such as the temperature sensor and accelerometer (if present).

A tarball containing the source code for the latest versions of these
test applications, as well as the Makefile required to build them, can
be found at Epiq Solutions' support website :ref:`[5] <ref5>`. A
Windows SDK for development under Windows is also available at Epiq
Solutions' support website :ref:`[5] <ref5>`. Assuming the GCC
compiler has been downloaded and installed, the steps for re-building
these applications for Linux are as follows:

1. Download the latest Sidekiq SDK tarball from :ref:`[5] <ref5>` to
   the Sidekiq PDK laptop. These tarballs have a version string in their
   filename. For example, version |release| of the Sidekiq SDK tarball will be
   called:

.. parsed-literal::
   sidekiq_sdk_v\ |release|\ .tar.xz

2. Untar the Sidekiq SDK tarball to a working directory on the Sidekiq
   PDK laptop. For example: if the development directory is located at
   ``/home/sidekiq/``, the following command would be used to
   extract its contents:

.. parsed-literal::

   $ tar xf /home/sidekiq/sidekiq_sdk_v\ |release|\ .tar.xz

3. Once the untar operation has been completed, the tarball's contents
   are available in the current working directory.

4. To re-build the test applications, change directories to the
   ``test_apps`` directory and run :code:`make`.

.. parsed-literal::

   $ cd /home/sidekiq/sidekiq_sdk_v\ |release|\ /test_apps
   $ make BUILD_CONFIG=x86_64.gcc clean
   $ make BUILD_CONFIG=x86_64.gcc

This will cause a complete re-build of all of the Sidekiq test
applications, with the resultant binary executable files located in
the ``.../test_apps/bin/`` directory. The applications built can
be executed directly on the Sidekiq laptop just like any other Linux
application. Most test applications can be executed without any
command line arguments, and will report their expected usage.  A user
may use the `--help` command line argument to force the display of
available arguments.

.. note:: The prebuilt applications and device drivers for alternative host
   systems (non-x86\_64) can be obtained from :ref:`[8] <ref8>`.

.. _table2:

.. table:: Linux Sidekiq SDK Tarball Directories
    :align: center
    :class: longtable

    +-----------------------------+
    | ::                          |
    |                             |
    |   sidekiq_sdk_vX.Y.Z        |
    |   |-- arg_parser            |
    |   | |-- inc                 |
    |   | `-- lib                 |
    |   |-- custom_xport_bare     |
    |   | |-- bin                 |
    |   | |-- src                 |
    |   | `-- test_apps           |
    |   |     `-- src             |
    |   |-- doc                   |
    |   |   |-- api               |
    |   |   |   `-- html          |
    |   |   |-- manual            |
    |   |   `-- sdk_manual        |
    |   |       `-- html          |
    |   |-- lib                   |
    |   |-- prebuilt_apps         |
    |   |-- sidekiq_core          |
    |   | `-- inc                 |
    |   `-- test_apps             |
    |    |-- bin                  |
    |    `-- src                  |
    +-----------------------------+

|

.. _table3:

.. table:: Linux Sidekiq SDK Tarball Files
    :align: center
    :class: longtable

    +---------------------------------------------------------+------------------------------------------------+
    | ::                                                                                                       |
    |                                                                                                          |
    |   sidekiq_sdk_vX.Y.Z/arg_parser/                                                                         |
    |   |-- inc                                                                                                |
    |   |   `-- arg_parser.h                                                                                   |
    |   `-- lib                                                                                                |
    |       `-- arg_parser__<BUILD_CONFIGs>.a                                                                  |
    +---------------------------------------------------------+------------------------------------------------+
    | ::                                                      | ::                                             |
    |                                                         |                                                |
    |   sidekiq_sdk_vX.Y.Z/custom_xport_bare/                 |   sidekiq_sdk_vX.Y.Z/sidekiq_core/             |
    |   |-- Makefile                                          |   `-- inc                                      |
    |   |-- src                                               |    |-- sidekiq_api.h                           |
    |   | `-- my_custom_xport.c                               |    |-- sidekiq_params.h                        |
    |   |-- test_apps                                         |    |-- sidekiq_types.h                         |
    |   | `-- src                                             |    |-- sidekiq_xport_api.h                     |
    |   |     `-- version_test.c                              |    `-- sidekiq_xport_types.h                   |
    |   `-- tools.mk                                          |                                                |
    +---------------------------------------------------------+------------------------------------------------+
    | ::                                                                                                       |
    |                                                                                                          |
    |   sidekiq_sdk_vX.Y.Z/doc/                                                                                |
    |   |-- api                                                                                                |
    |   |-- manuals                                                                                            |
    |   `-- sdk_manual                                                                                         |
    +---------------------------------------------------------+------------------------------------------------+
    | ::                                                                                                       |
    |                                                                                                          |
    |   sidekiq_sdk_vX.Y.Z/lib/                                                                                |
    |   |-- libsidekiq__<BUILD_CONFIGs>.a                                                                      |
    |   `-- support                                                                                            |
    |       `-- <BUILD_CONFIGs>/*                                                                              |
    +---------------------------------------------------------+------------------------------------------------+
    | ::                                                      | ::                                             |
    |                                                         |                                                |
    |   sidekiq_sdk_vX.Y.Z/prebuilt_apps/                     |   sidekiq_sdk_vX.Y.Z/test_apps/                |
    |   `-- x86_64.gcc                                        |   |-- Makefile                                 |
    |    |-- app_src_file1                                    |   |-- src                                      |
    |    |-- check_fpga_config                                |   |   |-- app_src_file1.c                      |
    |    |-- fdd_rx_tx_samples                                |   |   |-- elapsed.h                            |
    |    |-- multicard_dynamic_enable                         |   |   |-- fdd_rx_tx_samples.c                  |
    |    |-- multicard_rx_samples                             |   |   |-- multicard_dynamic_enable.c           |
    |    |-- multicard_tx_samples                             |   |   |-- multicard_rx_samples.c               |
    |    |-- prog_fpga                                        |   |   |-- multicard_tx_samples.c               |
    |    |-- read_accel                                       |   |   |-- prog_fpga.c                          |
    |    |-- read_gpsdo                                       |   |   |-- read_accel.c                         |
    |    |-- read_modules                                     |   |   |-- read_gpsdo.c                         |
    |    |-- read_temp                                        |   |   |-- read_imu.c (applicable to Z2 rev C)  |
    |    |-- ref_clock                                        |   |   |-- read_temp.c                          |
    |    |-- rx_benchmark                                     |   |   |-- rx_benchmark.c                       |
    |    |-- rx_samples                                       |   |   |-- rx_samples.c                         |
    |    |-- rx_samples_freq_hopping                          |   |   |-- rx_samples_freq_hopping.c            |
    |    |-- rx_samples_minimal                               |   |   |-- rx_samples_minimal.c                 |
    |    |-- rx_samples_on_trigger                            |   |   |-- rx_samples_on_trigger.c              |
    |    |-- set_rx_LO_freq                                   |   |   |-- set_rx_LO_freq.c                     |
    |    |-- sidekiq_probe                                    |   |   |-- sidekiq_probe.c                      |
    |    |-- store_user_fpga                                  |   |   |-- store_user_fpga.c                    |
    |    |-- sweep_receive                                    |   |   |-- sweep_receive.c                      |
    |    |-- tdd_rx_tx_samples                                |   |   |-- tdd_rx_tx_samples.c                  |
    |    |-- test_golden_present                              |   |   |-- test_golden_present.c                |
    |    |-- test_sample_rate                                 |   |   |-- tx_benchmark.c                       |
    |    |-- tx_benchmark                                     |   |   |-- tx_configure.c                       |
    |    |-- tx_configure                                     |   |   |-- tx_samples_async.c                   |
    |    |-- tx_samples                                       |   |   |-- tx_samples.c                         |
    |    |-- tx_samples_async                                 |   |   |-- tx_samples_from_FPGA_RAM.c           |
    |    |-- tx_samples_from_FPGA_RAM                         |   |   |-- tx_samples_on_1pps.c                 |
    |    |-- tx_samples_on_1pps                               |   |   |-- user_reg_test.c                      |
    |    |-- user_reg_test                                    |   |   |-- version_test.c                       |
    |    |-- version_test                                     |   |   `-- xcv_benchmark.c                      |
    |    `-- xcv_benchmark                                    |   `-- tools.mk                                 |
    +---------------------------------------------------------+------------------------------------------------+


.. _section_developing_apps:

************************************************
Developing Custom Applications with |libsidekiq|
************************************************

The Sidekiq SDK enables a user to develop their own software
applications that utilizes a Linux userspace library called
|libsidekiq|.  This library provides a high-level API for
configuring the RF transceiver, as well as transferring digitized
baseband I/Q samples between the CPU and the FPGA.

Structure of an Application using |libsidekiq|
==============================================

The ``.../test_apps/src/`` directory found in the Sidekiq SDK tarball
(currently ``sidekiq_sdk_v``\ |release_mono|\ ``.tar.xz``) contains the source code for several
example applications that utilize |libsidekiq|. Applications utilizing
|libsidekiq| should generally follow some basic guidelines to ensure they
operate properly. These guidelines are outlined below.

The general structure of a custom application should follow the template laid out in the
``.../sidekiq_sdk_v``\ |release_mono|\ ``/test_apps/`` directory. This provides an example of a
Makefile, as well as a directory to hold source files, header files (:code:`inc`), and output binary
executable files (:code:`bin`). It is certainly possible to use a different directory/file structure
here, but the included Makefile is set up to support this structure already. In addition, the
``app_src_file1.c`` (located in ``.../test_apps/src/``) source file contains the majority of the
typical structure found in a Sidekiq radio application.

Proper Header File Inclusion
============================

The entire |libsidekiq| API is exposed through a single header file called
``sidekiq_api.h``. This header file can be found at:

.. parsed-literal::

   .../sidekiq_sdk_v\ |release|\ /sidekiq_core/inc/sidekiq_api.h

Any application that utilizes |libsidekiq| will need to include the
``sidekiq_api.h`` header file in order to access the provided services.
This is the only header file that is required to be included.

There are four support header files, three of which are included
indirectly by ``sidekiq_api.h``. These are ``sidekiq_types.h``,
``sidekiq_params.h``, and ``sidekiq_xport_types.h``. The fourth support
header file is ``sidekiq_xport_api.h`` and is used by custom transport
developers when implementing registration of a custom transport. This
header file is found in the same location as ``sidekiq_api.h``:

.. parsed-literal::

   .../sidekiq_sdk_v\ |release|\ /sidekiq_core/inc/sidekiq_xport_api.h

Initializing |libsidekiq|
=========================

The |libsidekiq| library supports use of one or more Sidekiq cards in a
single host system. Each Sidekiq card can utilize either the PCIe
interface, the USB interface, or a custom defined interface for
transport between the host platform and the Sidekiq card. Automatic
detection of the interface is supported. The :code:`skiq_init()` function
is used to initialize the RFIC, initialize the |libsidekiq| library,
reserve the resources required for the specified Sidekiq card(s), and
initialize the communication transport. Inserting the kernel modules are
required prior to accessing a Sidekiq card when using a PCIe
transport. This is handled automatically on the PDK laptop and is
performed by the initialization script described in :ref:`installation_procedure`.

If it is desired to query how many Sidekiq cards are detected by the
host platform and the mapping of Sidekiq card (based on serial number)
to card number in the system prior to reserving the cards, the following
sequence of |libsidekiq| calls may be made:

.. code-block:: c

  uint8_t number_of_cards = 0;
  uint8_t card_list[SKIQ_MAX_NUM_CARDS];
  uint8_t card_num = 0;
  uint8_t i = 0;
  char *serial_str;

  /* query the list of all Sidekiq cards on the PCIe interface */
  skiq_get_cards( skiq_xport_type_pcie, &number_of_cards, card_list );

  for (i = 0; i < number_of_cards; i++)
  {
      /* determine the serial number based on the card number */
      skiq_read_serial_string( card_list[i], &serial_str );
      printf("Sidekiq card number %u has serial number %s\n", card_list[i], serial_str);

      /* determine the card number based on the serial number */
      skiq_get_card_from_serial_string( serial_str, &card_num );
      printf("Sidekiq serial number %s is located at card number %u\n",
             serial_str, card_num);
  }

A card can only be used by a single application at any point in
time. To determine a card’s availability, the
:code:`skiq_is_card_avail()` API should be used. If the card is
already in use, the process ID of the application using the card is
provided. When :code:`skiq_init()` is called for a specific card, the
card is reserved for use by that application and other applications
are denied access to the locked card.

To perform the initialization of |libsidekiq|, the :code:`skiq_init()`
function should be called specifying the cards to use and the
initialization level.

.. note:: As of |libsidekiq| v4.8.0, specification of the transport interface is
   deprecated and that :code:`skiq_xport_type_auto` is always used regardless of
   what is specified. The example below illustrates how to initialize two cards
   for full RF functionality.

.. code-block:: c

  uint8_t num_cards = 2;
  uint8_t cards[num_cards] = {0, 1};
  skiq_xport_type_t type = skiq_xport_type_auto;
  skiq_xport_init_level_t level = skiq_xport_init_level_full;

  /* initialize libsidekiq for card numbers 0 and 1 */
  skiq_init( type, level, cards, num_cards );

The initialization level specifies how much initialization should be
performed for each of the interfaces. Any application interested in
streaming sample data or configuring the RF radio properties should
perform a full initialization by specifying the level as
:code:`skiq_xport_init_level_full`.

The :code:`skiq_init()` function also initializes three mutexes per Sidekiq
card provided internally by |libsidekiq|: one mutex is used to control
access to FPGA registers, another mutex is used to protect access to the
FPGA → CPU interface used for receiving I/Q samples, and a third mutex is
used to protect access to the CPU → FPGA interface used for transmitting
I/Q samples. This allows a multi-threaded host application to access the
various |libsidekiq| services, while ensuring that calls to |libsidekiq| are
thread-safe.

.. note:: Running multiple applications that utilize |libsidekiq| is supported
   as long as each instance of |libsidekiq| is using a different Sidekiq
   card. Applications using the same Sidekiq card cannot be executed at the same
   time.

Lastly, as part of the initialization process, a signal handler should
be installed for capturing :code:`SIGINT` signals that may occur. Graceful
shutdown should be performed, including stopping any active streaming
and calling :code:`skiq_exit()`. Note that the stop streaming and exit
calls should not be made within the context of the signal handler as
those calls attempt to lock a mutex which may already be locked if a
receive or transmit call is in progress prior to the handling of the
signal. A recommended approach to handle the signal is to set a flag
which the main application checks to see if it should continue to run.
Upon the clearing of the "run flag", the application should cleanup and
call :code:`skiq_exit()`. See the ``tx_samples.c`` test application for an
example of this.

Dynamic Use of Sidekiq Cards
----------------------------

|Libsidekiq| v4.9.0 adds the ability for an application to dynamically
enable or disable use of Sidekiq cards independent of the library
initialization performed via the :code:`skiq_init()` API function. The
:code:`skiq_init_without_cards()` API function may be used to initialize
the library without reserving any card for immediate use; this is the
equivalent of calling :code:`skiq_init()` with an empty card list.

At a later point during the application’s execution, a card can be
initialized and enabled for use through either the
:code:`skiq_enable_cards()` or the
:code:`skiq_enable_cards_by_serial_str()` API function. This will
perform a full initialization of the card and reserve the card’s use to
the requesting application. At a later point in time, if the card is no
longer needed by the application, the card can be released using the
:code:`skiq_disable_card()` API. This shuts down the card, and this card
is the considered available for use by either the same or another
application. Calling :code:`skiq_exit()` performs a full shutdown of the
library as well as any card currently reserved by the application. There
is no need to perform an additional call to :code:`skiq_disable_card()`
prior to calling :code:`skiq_exit()`.

A simple example (without error checking) of accessing cards dynamically
could be:

.. code-block:: c

  uint8_t num_cards = 0;
  uint8_t cardList[SKIQ_MAX_NUM_CARDS] = { 0 };

  /* Initialize libsidekiq with no cards - none are needed right now */
  skiq_init_without_cards();

  /* Do some tasks */
  ...

  /* A Sidekiq card is now needed; dynamically enable card 0 */
  cardList[0] = 0;
  num_cards = 1;
  skiq_enable_cards(cardList, num_cards, skiq_xport_init_level_full);

  /* Perform some radio tasks */
  ...

  /* Done with the card; dynamically disable card 0 */
  cardList[0] = 0;
  num_cards = 1;
  skiq_disable_cards(cardList, num_cards);

  /* Perform some cleanup tasks */
  ...

  /* Shutdown libsidekiq */
  skiq_exit();


The test application ``multicard_dynamic_enable.c`` provides a basic
example of dynamically enabling or disabling a card for use by the
application.

Logging
-------

By default, |libsidekiq| executes any logging of the library with syslog.
The default configuration is to display any logging message to both the
console as well as within ``/var/log/syslog``. A user can utilize their
own logging function by registering their own logging function pointer
with the :code:`skiq_register_logging()` API function. Refer to the
``app_src_file1.c`` test application for an example of this. If the user
is interested in completely disabling any logging or prints within the
Sidekiq library, :code:`NULL` can be provided to
:code:`skiq_register_logging()`.

Transport Layer
---------------

Within |libsidekiq|, there is the concept of a "transport" (or xport)
layer. This layer facilitates I/O between a Sidekiq card and host,
such as configuring radio parameters via register transactions and
streaming I/Q samples. Sidekiq’s hardware by default is configured to
implement PCIe and requires only that software be initialized in the
manner as described above. The Sidekiq mPCIe and m.2 radios additionally
have an available USB transport; making use of this transport
is only recommended for use in systems where PCIe is not feasible, as
the USB transport is significantly less performant when streaming
samples and transacting configuration registers. Both Sidekiq Z2 and
Matchstiq Z3u utilize a custom transport and the transport-specific
FPGA bitstream is automatically loaded upon boot for these radios. For
details on configuring the FPGA, refer to :ref:`fpga_programming`.

.. note:: Sidekiq X2, X4, Stretch, and NV100 do not support the USB transport layer.

Parameters
----------

Depending on the Sidekiq product and configuration, availability of
certain hardware peripherals may be limited. Additionally, configuration
of various RF parameters such as frequency, sample rate, and gain, vary.
To determine the parameters and ranges of a specific Sidekiq card, the
:code:`skiq_read_parameters()` API should be used. The parameters
available to query are defined in :code:`skiq_param_t`.

Configuring an Interface using a Handle
=======================================

Each of the underlying RF interfaces presents itself as a handle that
can be used to reference the desired RF interface for purposes of
configuration and data transfer. The |libsidekiq| API provides read/write
access functions for a variety of radio configuration parameters. 
The full listing of these access functions
can be found in the Sidekiq API documentation
(``sidekiq_sdk_current/doc/api/Sidekiq_API_``\ |version_mono|\ ``.0.pdf``) as well
as in ``sidekiq_api.h``.

Handles sharing the same letter (A1/A2) or (B1/B2) indicates a phase-coherent relationship and shared 
resources, mainly a shared local oscillator (LO) and clocking.  Consequently, 
adjusting the sample rate or center frequency on handle A1 will also impact the sample
rate on A2, and vice versa.  Despite this dependence, each Rx interface has its own independently 
configurable RF lineup including gain, overload detection, etc.

Please see the table below containing Sidekiq models, supported handles, and usage restrictions.

.. list-table::
   :header-rows: 1
   :widths: 20 20 40

   * - Sidekiq Variant
     - Supported Rx Handles
     - Notes
   * - mPCIe
     - A1
     - 
   * - mPCIe (-001 variant)
     - A1, A2
     - 
   * - m.2
     - A1, A2
     - 
   * - X2
     - A1, A2, B1
     - 
   * - X4
     - A1, A2, B1, B2, C1, D1
     - | Neither C1/A2 nor D1/A2 support simultaneous operation.
       |
       | 4 channel phase coherent Rx (A1,A2,B1,B2) is supported.
   * - Sidekiq Stretch
     - A1
     - 
   * - NV100
     - A1, A2, B1
     - A2 and B1 cannot be used simultaneously.
   * - Matchstiq Z3u
     - A1, A2
     - 

.. note::

   If streaming is attempted for conflicting streams, the start streaming call will fail 
   with :code:`-EBUSY`. To determine conflicting handles, the 
   :code:`skiq_read_rx_stream_handle_conflict()` API function can be used.
An example sequence for configuring a sub-set of the available
parameters for multiple Rx interface handles is shown below.

.. code-block:: c

  result = skiq_write_rx_sample_rate_and_bandwidth(card, hdl_a1, sample_rate, bandwidth);
  if ( result != 0 )
  {
    printf("Error: failed to set sample rate to %u Hz on RxA1\n", sample_rate);
    return(-1);
  }

  result = skiq_write_rx_sample_rate_and_bandwidth(card, hdl_a2, sample_rate, bandwidth);
  if ( result != 0 )
  {
    printf("Error: failed to set sample rate to %u Hz on RxA2\n", sample_rate);
    return(-1);
  }

  result = skiq_write_rx_LO_freq(card, hdl_a1, lo_freq);
  if ( result != 0 )
  {
    printf("Error: failed to set Rx LO freq to %" PRIu64 " Hz on RxA1\n", lo_freq);
    return(-1);
  }

  result = skiq_write_rx_gain_mode(card, hdl_a1, skiq_rx_gain_manual);
  if ( result != 0 )
  {
    printf("Error: failed to set Rx gain mode to manual on RxA1\n");
    return(-1);
  }

  result = skiq_write_rx_gain_mode(card, hdl_a2, skiq_rx_gain_manual);
  if ( result != 0 )
  {
    printf("Error: failed to set Rx gain mode to manual on RxA2\n");
    return(-1);
  }

  result = skiq_read_rx_gain_index_range(card, hdl_a1, &gain_index_min, &gain_index_max);
  if ( result != 0 )
  {
    printf("Error: failed to read Rx gain index on RxA1\n");
    return(-1);
  }
  rx_a1_gain = gain_index_min;

  result = skiq_read_rx_gain_index_range(card, hdl_a2, &gain_index_min, &gain_index_max);
  if ( result != 0 )
  {
    printf("Error: failed to read Rx gain index on RxA2\n");
    return(-1);
  }
  rx_a2_gain = gain_index_max;

  result = skiq_write_rx_gain(card, hdl_a1, rx_a1_gain);
  if ( result != 0 )
  {
    printf("Error: failed to set Rx gain to %u on RxA1\n", rx_a1_gain);
    return(-1);
  }

  result = skiq_write_rx_gain(card, hdl_a2, rx_a2_gain);
  if ( result != 0 )
  {
    printf("Error: failed to set Rx gain to %u on RxA2\n", rx_a2_gain);
    return(-1);
  }

All access functions return an :code:`int32_t` with a status code, where
0 = success and any other value indicates an error that occurred.

Frequency Hopping
=================

As of |libsidekiq| v4.10, the ability to rapidly hop amongst a user
pre-defined frequency list is supported for most products. Examples of
how to use the frequency hopping APIs are provided in the
:code:`tx_configure` and :code:`rx_samples_freq_hopping` test applications.

The RFICs used on Sidekiq X4 and libsidekiq impose a restriction on how to perform frequency
hopping.  A hop index can only be written to a "mailbox" slot on the RFIC.  A hop operation executes
a retune to the frequency in the "next" slot, then delivers the index from the "mailbox" to the
"next" slot.  It acts much like a 2 element deep FIFO that must have 1 or 2 indices enqueued and
cannot go empty.  Even though this approach is only required for Sidekiq X4, |libsidekiq| presents a
consistent interface across all radio products.

The general API flow (without error checking) to configure and utilize frequency hopping are shown
in the figure below. Details of the API functions related to frequency hopping are outlined in the
sections that follow.

.. code-block:: c

 skiq_rx_hdl_t hdl = skiq_rx_hdl_A1;
 uint8_t card = 0;
 uint8_t num_hop_freqs = 5;
 uint8_t initial_hop_idx = 0;
 uint64_t freq_list[num_hop_freqs] = { 100000000, 200000000, 300000000, 400000000, 500000000 };

 /* an RF timestamp of 0 indicates an "immediate" hop since 0 is always in the past */
 uint64_t rf_timestamp = 0;

 /* configure frequency tune mode for "immediate" */
 skiq_write_rx_freq_tune_mode( card, hdl, skiq_freq_tune_mode_hop_immediate );

 /* configure the hopping frequency list and the initial hop index */
 skiq_write_rx_freq_hop_list( card, hdl, num_hop_freqs, freq_list, initial_hop_idx );

 /* prepare the next hop to follow the initial hop index to be at freq_list[3] */
 skiq_write_next_rx_freq_hop( card, hdl, 3 );

 /* immediately execute the hop to the initial frequency (i.e. freq_list[initial_hop_idx]) */
 skiq_perform_rx_freq_hop( card, hdl, rf_timestamp );

 /* prepare the next hop to follow the freq_list[3] to be at freq_list[1] */
 skiq_write_next_rx_freq_hop( card, hdl, 1 );

 /* immediately execute the hop to the second frequency (i.e. freq_list[3]) */
 skiq_perform_rx_freq_hop( card, hdl, rf_timestamp );
 
 /* at this point, there is one hop left in the RFIC's hop FIFO: freq_list[1].  In order to hop to
    that frequency, however, a frequency index *must* first be written to the "mailbox" slot using
    skiq_write_next_rx_freq_hop() or the FIFO will go empty (which is not allowed) */

Configuring Tune Mode
---------------------

The tune mode can be configured to hop on timestamp
(:code:`skiq_tune_mode_hop_on_timestamp`), hop immediately
(:code:`skiq_tune_mode_hop_immediate`), or to use the default of the
"standard" tuning (:code:`skiq_tune_mode_standard`).

The mode can be configured with :code:`skiq_write_rx_freq_tune_mode()`
/ :code:`skiq_write_tx_freq_tune_mode()`. Additionally, the current
mode can be queried with :code:`skiq_read_rx_freq_tune_mode()` /
:code:`skiq_read_tx_freq_tune_mode()`.  Note that while Sidekiq X2's
RFIC does not support fast frequency hopping, the API to perform the
LO tuning is supported when using :code:`skiq_tune_mode_hop_immediate`
(as of |libsidekiq| v4.12.0).

When retuning with the frequency hopping API in either the
:code:`skiq_tune_mode_hop_on_timestamp` or
:code:`skiq_tune_mode_hop_immediate` tuning mode, the RFIC’s calibration algorithms
are not performed to support faster tuning. If rapid tuning is desired
at the cost of reduced RF performance, the frequency hopping tune mode
is recommended. However, if maximum reduction of DC offset or image is
desired, then the standard tuning mode is recommended.

Frequency List Definition
-------------------------

When using frequency hopping, a list of frequencies must be specified
prior to executing a hop to a specific frequency. The maximum number of
frequencies that can be specified is limited to
:code:`SKIQ_MAX_NUM_FREQ_HOPS`.

The frequency list can be defined with
:code:`skiq_write_rx_freq_hop_list()` /
:code:`skiq_write_tx_freq_hop_list()`. A previously specified frequency
list can be queried with :code:`skiq_read_rx_freq_hop_list()` /
:code:`skiq_read_tx_freq_hop_list()`.

Prepare Next Frequency Hop
--------------------------

The next frequency to hop to **must** be configured prior to executing the
frequency hop using either the
:code:`skiq_write_next_rx_freq_hop()` /
:code:`skiq_write_next_tx_freq_hop()` API calls. To determine the details
of the previously configured "next hop", the
:code:`skiq_read_next_rx_freq_hop()` /
:code:`skiq_read_next_tx_freq_hop()` API calls can be used.  When a hop is
executed with :code:`skiq_perform_rx_freq_hop()` / :code:`skiq_perform_tx_freq_hop()`,
this will be the frequency that is configured.

Execute Frequency Hop
---------------------

As long as there was a previously configured "next hop", the "perform
hop" APIs can be used to execute the frequency hop. In the case of
:code:`skiq_tune_mode_hop_immediate`, the frequency hop is executed
immediately. In the case of :code:`skiq_tune_mode_hop_on_timestamp`, the
frequency hop is not initiated until the specified timestamp has been
reached; if a timestamp in the past is specified, then the frequency hop
occurs immediately. The API to complete the frequency hop is
:code:`skiq_perform_rx_freq_hop()` / :code:`skiq_perform_tx_freq_hop()`.
The :code:`skiq_read_curr_rx_freq_hop()` / :code:`skiq_read_curr_tx_freq_hop()` API functions can be
used to query the details of the currently configured frequency.

.. note:: Hopping on timestamp uses GPIO pins between the FPGA and RFIC to
   execute the frequency hop. If these pins are already in use with a custom
   FPGA design, unexpected behavior may occur.

Operation Modes
===============

Sidekiq can support 3 different modes of operation: single channel
receiver (Rx A/B/C/D 1 only), dual channel receiver (both Rx A/B1 and
Rx A/B2), or single channel transceiver (Rx A1 / Tx A1).  With the Sidekiq-002,
Sidekiq Z2 products, and Sidekiq m.2 2280 only a single channel transceiver
is available. For Matchstiq Z3u, either a single channel transceiver is supported
or a dual channel receiver mode can be used.  The operation mode is specified by
configuring the channel mode:

.. code-block:: c

  /* configure mode for both Rx A/B1 and Rx A/B2 */
  skiq_write_chan_mode(card, skiq_chan_mode_dual);

If only Rx A1/B1/C1/D1 is needed for receiving or if transmitting, the mode
should be set to :code:`skiq_chan_mode_single`. This mode should be
configured prior to starting an interface or undefined behavior may
occur.

The Sidekiq m.2, Sidekiq X2, Sidekiq X4, and Sidekiq NV100 products
additionally support a dual channel transceiver (Rx A/B 1/2 and Tx A/B
1/2). The :code:`skiq_write_chan_mode()` API call may be used as well to
configure the channel mode.

Finally, both Sidekiq X4 and Sidekiq NV100 are each based on different RFICs found on the other
Sidekiq products. These RFICs are designed primarily for TDD usage. This means that Rx and Tx cannot
be streaming at the same time for the A* or B* handles. Note that streaming TX for an A* handle can
happen simultaneously while receiving on a B* handle.  Additionally, the LO frequency is shared
across Rx and Tx frequencies.  In the case of Sidekiq X4, :code:`skiq_rx_hdl_A1` /
:code:`skiq_rx_hdl_A2` share the LO frequency with :code:`skiq_tx_hdl_A1` / :code:`skiq_tx_hdl_A2`
and :code:`skiq_rx_hdl_B1` / :code:`skiq_rx_hdl_B2` share the LO frequency with
:code:`skiq_tx_hdl_B1` / :code:`skiq_tx_hdl_B2` for the Sidekiq X4 product.  In the case of Sidekiq
NV100, :code:`skiq_rx_hdl_A1` / :code:`skiq_rx_hdl_A2` share an LO, and 
:code:`skiq_tx_hdl_A1`/:code:`skiq_tx_hdl_B1` share an LO with :code:`skiq_rx_hdl_B1`. The ability 
to adjust the Tx LO selection will be supported in a future libsidekiq version.



RF Port Configuration
=====================

Certain versions of the Sidekiq product support configuration of the RF
port mode to perform either transmission or reception of sample data
(referred to as TRx operation). Alternatively, the Sidekiq card operates
in fixed mode, where the RF ports cannot be switched between receive and
transmit. A detailed table outlining options and configurations per
Sidekiq is captured in :ref:`rf_port_config`.

Certain versions of the Sidekiq card also support configuration of the
RF port to RF handle mapping. Note that only specific RF ports are
available in the fixed or TRx mode. To determine RF ports available for
a specific handle, the following API can be used:

.. code-block:: c

  /* read the RX ports available for Rx A1 */
  skiq_read_rx_rf_ports_avail_for_hdl(card,
                                   skiq_rx_hdl_A1,
                                   &num_fixed_rf_ports,
                                   fixed_rf_ports,
                                   &num_trx_rf_ports,
                                   trx_rf_ports );

  /* read the TX ports available for Tx A1 */
  skiq_read_tx_rf_ports_avail_for_hdl(card,
                                   skiq_tx_hdl_A1,
                                   &num_fixed_rf_ports,
                                   fixed_rf_ports,
                                   &num_trx_rf_ports,
                                   trx_rf_ports );

The currently configured RF port can be queried and optionally
configured, as shown below.

.. code-block:: c

  /* read the configured RF port for Rx A1 */
  skiq_read_rx_rf_port_for_hdl(card,
                            skiq_rx_hdl_A1,
                            &rf_port );

  /* read the configured RF port for Tx A1 */
  skiq_read_tx_rf_port_for_hdl(card,
                            skiq_tx_hdl_A1,
                            &rf_port );

  /* write the configured RF port for Rx A1 */
  skiq_write_rx_rf_port_for_hdl(card,
                                 skiq_rx_hdl_A1,
                                 skiq_rf_port_J2 );

  /* write the configured RF port for Tx A1 */
  skiq_write_tx_rf_port_for_hdl(card,
                                skiq_tx_hdl_A1,
                                skiq_rf_port_J1 );

The RF port modes available for a specific Sidekiq can be queried
directly as shown below, or accessed via the :code:`skiq_params_t`.

.. code-block:: c

  /* determine the RF port configuration available */
  skiq_read_rf_port_config_avail( card, &fixed_mode, &trx_mode );

Additionally, some variants of Sidekiq support updating the RF port
configuration dynamically. The current RF port configuration can be read
by:

.. code-block:: c

  /* determine the current RF port configuration */
  skiq_read_rf_port_config( card, &rf_port_config );

Finally, when operating with the RF configuration of
:code:`skiq_rf_port_config_trx`, the mode of RF port can switch between
receive and transmit with the following API.

.. code-block:: c

  /* switch the operation of the RF port to transmit */
  skiq_write_rf_port_operation( card, true );

  /* switch the operation of the RF port to receive */
  skiq_write_rf_port_operation( card, false );

For more details on the modes available for a specific hardware variant
of Sidekiq, please contact Epiq Solutions support :ref:`[5] <ref5>`.
For an example of switching between receive and transmit modes, refer to
the ``tdd_rx_tx_samples.c`` test application.

I/Q Ordering Mode
=================

The order in which complex samples are received or transmitted by
Sidekiq is a configurable option. Ordering can be adjusted by the user
at run-time *before* streaming is started by means of the
:code:`skiq_write_iq_order_mode()` function and the :code:`skiq_iq_order_t`
type. If not specified, Sidekiq will operate with :code:`skiq_iq_order_qi`,
as depicted in the :ref:`_table_rx_packet_structure` and
:ref:`figure_tx_packet_structure` figures. In this mode, Q0 is data[0],
I0 is data[1], Q1 is data[2], I1 is data[3], and so on.

Alternatively in the case of :code:`skiq_iq_order_iq`, the order will be
swapped: I0 is data[0], Q0 is data[1], I1 is data[2], Q1 is data[3], and
so on. For an example of adjusting the ordering mode, please refer to
the test application ``rx_samples.c`` or ``tx_samples.c``.

Packed Mode (Sidekiq mPCIe, m.2, and Stretch / m.2-2280 only)
=============================================================

By default, Sidekiq radios operate in 'unpacked' mode where each I or Q sample
is stored in its own 16-bit value. However, not all radios use all 16-bits for
sample data - see below table for more information. To query the supported I/Q
sample size (resolution) using the |libsidekiq| API, use
:code:`skiq_read_rx_iq_resolution()` and :code:`skiq_read_tx_iq_resolution()`.

.. _table_unpacked_size

.. table:: Unpacked sample sizes per radio
    :align: center

    ===============  ========================  ========================
    Radio            RX sample size            TX sample size
                     (in bits)                 (in bits)
    ===============  ========================  ========================
    Sidekiq mPCIe    12 :sup:`*`               12 :sup:`*`
    Sidekiq m.2      12 :sup:`*`               12 :sup:`*`
    Sidekiq Stretch  12 :sup:`*`               12 :sup:`*`
    Sidekiq Z2       12 :sup:`*`               12 :sup:`*`
    Matchstiq Z3u    12 :sup:`*`               12 :sup:`*`
    Sidekiq X2       16                        14 (upper)
    Sidekiq X4       16                        14 (upper)
    Sidekiq NV100    16                        16
    ===============  ========================  ========================

:sup:`*` -- sample is sign extended to 16 bits

Additionally, Sidekiq mPCIe, Sidekiq m.2, and Sidekiq Stretch also support a
mode of operation that compacts the I/Q samples to use 12-bits instead of
16-bits per I and Q sample. This mode is referred to as packed mode. Packed
mode is useful when it is desired to operate at a sample rate higher than the
rate at which the transport interface can reliably transfer data. This allows
for more samples to be transferred within a given time period at the cost of
requiring unpacking of the samples prior to consumption when receiving or
packing of the samples while transmitting. Packed mode can be enabled or
disabled by calling the :code:`skiq_write_iq_pack_mode()` function. The mode
applies to both transmitting and receiving samples. When packed mode is
enabled, the sample data should be formatted as shown in
:ref:`figure_packed_iq_structure`. The format of the metadata remains the same
regardless of whether packed mode is enabled or not.

.. _figure_packed_iq_structure:
.. figure:: _static/packed_iq_structure.png
   :alt: Sidekiq Packed Sample Structure
   :align: center
   :width: 5.11930in
   :height: 1.39290in

   Sidekiq Packed Sample Structure

Starting an Rx Interface
========================

Once an Rx interface has been properly configured for operation, the
interface can then be started to begin data flowing between the CPU and
FPGA. The process of starting an Rx interface allows a user to specify
the Rx interface handle to start. An example of starting an Rx interface
is shown below.

.. code-block:: c

  /* begin streaming on the Rx interface */
  result = skiq_start_rx_streaming(card, hdl);

Reading I/Q Samples from an Rx Interface
----------------------------------------

Once an Rx interface has started streaming, a block of contiguous I/Q
samples can be read from the Rx interface using the :code:`skiq_receive()`
function. This function accepts a pointer to a :code:`skiq_rx_block_t`
pointer that will then be populated with the address of where the
metadata and I/Q data block is stored in the DMA engine provided by
|libsidekiq|. No memory copy operation is performed here when using the PCIe
transport, just an update of the passed in pointer, to maximize
efficiency of accessing the data. Note that this memory is controlled by
the DMA interface and is not reserved for the application. Thus, if the
data is not copied or processed prior to the DMA interface requiring
that memory for new sample data, there is the potential for the sample
data to be overwritten. The sample rate of each of the active interfaces
will determine how quickly the DMA engine fills up the and overwrites
previous sample data. To check for an overflow condition, the RF pair
timestamp should be monitored (as discussed in the below paragraphs and
shown in the code example).

.. note:: Each successful call to :code:`skiq_receive()` will only return a
   single packet.

The default behavior of :code:`skiq_receive()` function is non-blocking.
If there is no new data available, the function will return immediately
with a status of :code:`skiq_rx_status_no_data`. For details on how to
configure and use the blocking receive capabilities, refer to
:ref:`Developing Custom Applications with |libsidekiq|`. In the default case
of non-blocking receive,the application is responsible for polling the
receive interface for packets. There is some overhead associated with calling
:code:`skiq_receive()` when no new data is available, so it is recommended
to throttle the calls based on when sample data is expected to be
available or utilize the blocking receive capability. Additionally, when
multiple Rx handles are enabled, the packets of sample data from each of
the handles may be interleaved, and the :code:`p_hdl` parameter of the
:code:`skiq_receive()` function is populated with the source handle from
which the packet was received. The contents of :code:`skiq_receive()`
arguments are valid only when :code:`skiq_rx_status_success` is returned.

The :code:`skiq_rx_block_t` structure has fields for the I/Q data and
elements of the metadata in the header. Note: future versions of
|libsidekiq| may adjust this header size, so there is a ``#define`` called
:code:`SKIQ_RX_HEADER_SIZE_IN_WORDS` which specifies the size of the
header. The metadata contains a timestamp which is incremented at the
same rate as the sample rate of that Rx interface. This field is called
:code:`rf_timestamp` in the :code:`skiq_rx_block_t` struct. Additionally, the
phase coherent RF pair (Rx A1/2 or Rx B1/2) uses the same timestamp
reference, or in the case of Sidekiq X4, all 4 phase coherent receivers
use the same timestamp reference. This allows the samples from Rx A1 to
be phase aligned with samples from Rx A2 (or in the case of Sidekiq X4,
Rx A1/A2/B1/B2 are all phase aligned). The metadata also contains the
system timestamp associated with the packet (named :code:`sys_timestamp`).
The system timestamp is maintained independent of the sample rate and is
used across all of the Rx interfaces. The system metadata (currently
available only with PCI transport) comprises of the RF IC control output
bits, the Rx overload status (not available with all products), and the
data source (handle). The data source correlates to the
:code:`skiq_rx_hdl_t` definition. The Rx overload bit is set if the Rx
overload detection was active for that packet (not available with all
products). The RF IC control output bits are defined by both the RF IC
control output mode and enable; configured by the
:code:`skiq_write_rfic_control_output_config()` API. For details on the
modes available, please refer to the "Control Output" section of
:ref:`[9] <ref9>` (for Sidekiq mPCIe / m.2 / m.2-2280 / Z2 / Z3u), "Monitor Output"
section of :ref:`[10] <ref10>` (for Sidekiq X2), or "GPIO Monitor
Mode" of :ref:`[11] <ref11>` (for Sidekiq X4). Note that the
``rx_samples.c`` test application provides an example of configuring the
mode such that the gain of RxA1 is present in the metadata. The user
metadata is a 32-bit field available for custom FPGA applications to use
if desired. The ':code:`data`' field of the :code:`skiq_rx_block_t` struct
consists of I/Q sample data. The fields of the example I/Q packet is shown
below. In some applications it may be desirable to change the ordering
of the complex (I/Q) samples. For details on how to do so, please refer
to :ref:`I/Q Ordering Mode`

.. table:: Rx I/Q Packet Structure
    :name: _table_rx_packet_structure
    :align: center
    :widths: 20 10 70

    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | Field                  | Width (bits)   | Description                                                                                                 |
    +========================+================+=============================================================================================================+
    | :code:`rf_timestamp`   | 64             | RF timestamp associated with the received sample block                                                      |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`sys_timestamp`  | 64             | System timestamp associated with the received sample block                                                  |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`hdl`            | 6              | Receive handle indicating the receive handle associated with the received sample block                      |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`overload`       | 1              | RF Overload indicating whether or not the RF input was overloaded for the received sample block             |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`rfic_control`   | 8              | RFIC control word carries metadata from the RFIC, typically the receive gain index                          |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`id`             | 8              | Channel ID used by channelizer (currently unused)                                                           |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`system_meta`    | 6              | System metadata (unused / reserved)                                                                         |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`version`        | 3              | Packet version field                                                                                        |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`user_meta`      | 32             | User metadata typically populated by a custom FPGA build                                                    |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+
    | :code:`int16_t data[]` | 16             | array of unpacked I/Q samples (16 bits per I or Q sample).                                                  |
    |                        |                | Q\ :sub:`0` is data[0], I\ :sub:`0` is data[1], Q\ :sub:`1` is data[2], I\ :sub:`1` is data[3], and so on.  |
    +------------------------+----------------+-------------------------------------------------------------------------------------------------------------+


.. note:: If a sufficiently high sample rate is selected (typically in the range
   of ~50 Msamples/sec - equivalent to 200 MB/sec - for Sidekiq mPCIe, dependent
   on the capabilities of the host platform), gaps in the timestamps may occur
   due to either:

    A) the |libsidekiq| software ring buffer filling up, or

    B) the throughput rate of the FPGA → CPU interface being exceeded.

    For applications that require continuous data flow, it is
    imperative to confirm that the timestamp is monotonically increasing at
    the expected rate to ensure no data is dropped. For recommendations on
    how to evaluate the Rx throughput performance for a particular host
    system, refer to :ref:`Receive Performance`.

An example loop that does nothing but read blocks of I/Q data and
verifies the timestamps (to ensure that no gaps exist in the data stream) is shown below.

.. code-block:: c

  /* loop through and acquire the requested # of i/q sample blocks, verifying
      that the timestamp (ts) increments as expected */
  while( num_blocks < tot_num_blocks )
  {
    status = skiq_receive(card, &hdl, &p_rx_block, &len);
    if( status == skiq_rx_status_success )
    {
      if( p_rx_block != NULL )
      {
        curr_ts = p_rx_block->rf_timestamp;

        if( first_block == true )
        {
          first_block = false;
          next_ts = curr_ts;
          next_ts += ((len/4) - SKIQ_RX_HEADER_SIZE_IN_WORDS);
        }
        else if( curr_ts != next_ts )
        {
          printf("Error: timestamp error expected 0x%016" PRIx64 " but got 0x%016" PRIx64
                 ".\n", next_ts, curr_ts);

          return (-1);
        }
        else
        {
          next_ts += ((len/4) - SKIQ_RX_HEADER_SIZE_IN_WORDS);
        }
      }

      num_blocks++;
    }
  }

Lastly, most parameters associated with a given Rx interface can be
updated after the interface has been started. It is not necessary to
stop the interface to re-configure an interface. The only exceptions
here are that changes to the channel mode, packed mode, I/Q order mode, and data source
mode must be configured prior to starting the interface. Also, a change
to sample rate will result in the data flow being automatically stopped and restarted
once the sample rate has been applied.

Counter Mode with Rx Interface
------------------------------

An Rx interface can be configured to either provide I/Q sample data or
counter data. The normal mode of operation is using the I/Q sample mode.
However, counter mode can be useful in various test scenarios. The type
of data provided can be configured with the
:code:`skiq_write_rx_data_src()` function. The data source can be
updated at any time but is only applied when streaming is started. When
in counter mode, each sample is a 12-bit value for Sidekiq mPCIe, m.2,
m.2-2280, Z2 and Matchstiq Z3u and 16-bit for Sidekiq X2, X4, and NV100. When running in counter
mode on the Sidekiq, the I sample is odd while the Q sample is an even
number. For details on how to validate the counter data, refer to the
:code:`verify_data()` function in the ``rx_samples.c`` test application.

Making :code:`skiq_receive` a blocking call
-------------------------------------------

As of |libsidekiq| v3.3.0, a call to :code:`skiq_receive()` can be configured
to block until an I/Q sample block is available instead of returning
immediately. Using a blocking infrastructure can save CPU cycles and
provide other processes with time to execute. The addition of a blocking
receive call also provides flexibility in how |libsidekiq| may be
leveraged for different use cases.

.. note:: Not all transports support blocking receive.

The arguments to :code:`skiq_receive()` stay the same and |libsidekiq|
continues to default to non-blocking for :code:`skiq_receive()`. The
:code:`skiq_set_rx_transfer_timeout()` API function allows a developer
to specify how :code:`skiq_receive()` behaves when samples are not
available. For example, if the developer specifies
:code:`RX_TRANSFER_NO_WAIT` to :code:`skiq_set_rx_transfer_timeout()`, a
call to :code:`skiq_receive()` returns immediately if samples are not
available (this is the default behavior). A developer may specify a
timeout between 20uS and 1000000uS to
:code:`skiq_set_rx_transfer_timeout()`. In this configuration, a call to
:code:`skiq_receive()` will return after the specified timeout has elapsed
if samples are not available. If the developer specifies
:code:`RX_TRANSFER_WAIT_FOREVER` as the timeout, a call to :code:`skiq_receive()`
will block indefinitely until samples are available. In both the timeout
and :code:`RX_TRANSFER_WAIT_FOREVER` configurations, :code:`skiq_receive()`
returns once samples are available at the next opportunity the kernel
provides to the associated process.

.. note:: For improved CPU usage and efficiency in receiving, a non-zero timeout
   is recommended.  Additionally, a timeout that is greater than the
   inter-sample-block timing at the configured Rx sample rate is also
   recommended. In most cases, a timeout of 25000uS is sufficient to reap the
   benefits of a blocking receive.

.. caution:: when using a non-zero timeout, calling
   :code:`skiq_stop_rx_streaming()` or :code:`skiq_exit()` from
   another thread can cause :code:`skiq_receive()` to return without a
   packet. Be sure to handle that case.

If desired to configure a timeout for the receive call, the API
:code:`skiq_get_rx_transfer_timeout()` provides the timeout value or a
pre-processor define (:code:`RX_TRANSFER_NO_WAIT`,
:code:`RX_TRANSFER_WAIT_FOREVER`, or :code:`RX_TRANSFER_WAIT_NOT_SUPPORTED`).
The value :code:`RX_TRANSFER_WAIT_NOT_SUPPORTED` is provided in cases
where the transport layer (currently custom or USB) does not support a
blocking receive infrastructure.

For an example application that optionally makes use of the blocking
receive capabilities, refer to the ``rx_samples.c`` test application.

Using receive calibration offsets
---------------------------------

With the introduction of |libsidekiq| v4.4.0, Sidekiq units leaving the
factory have a per-unit receiver calibration data stored in non-volatile
memory. There are several API functions (introduced in |libsidekiq|
v4.0.0) that provide a receive calibration offset based on the RF
configuration that may be applied by the user to calculate the
calibrated RF energy present in a block of I/Q samples. If a unit does
not have stored calibration data, the API functions will fall back to a
default data set that represents a given product line (miniPCIe vs m.2
vs Z2, etc), receive handle (:code:`skiq_rx_hdl_t`), and RF port
(:code:`skiq_rf_port_t`). The four API functions that provide a calibration
offset are as follows.

-  :code:`skiq_read_rx_cal_offset()`
-  :code:`skiq_read_rx_cal_offset_by_gain_index()`
-  :code:`skiq_read_rx_cal_offset_by_LO_freq()`
-  :code:`skiq_read_rx_cal_offset_by_LO_freq_and_gain_index()`

These functions provide varying degrees of control when querying for a
receive calibration offset. If a parameter is not specifiable, it is
taken from the present RF configuration.

In order to use the receive calibration offset :math:`G_{radio}`, a user should
first calculate the baseband power in dB (:math:`P_{bb}`), then subtract the calibration
offset (returned in units of dB) to calculate the RF power (:math:`P_{rf}`) of the signal
in dBm. The equations below further describe how to apply :math:`G_{radio}`:

.. _receive_calibration_math:

.. math::
    :nowrap:

    \begin{gather*}
    P_{bb} = 10\ log_{10}\left(\frac{1}{N}\ \displaystyle \sum_{k=0}^{N} i_k^2 + q_k^2\right)\\
    P_{rf} = P_{bb} - G_{radio}
    \end{gather*}

Using I/Q phase and amplitude calibration
-----------------------------------------

With the introduction of |libsidekiq| v4.7.0, the Sidekiq X4 FPGA design
provides in-line complex multipliers for each receiver handle (Rx A1, Rx
A2, Rx B1, and Rx B2; not available with Rx C1/D1) to allow a user to
reduce phase and amplitude differences among the receivers. At this
release, the Sidekiq X4 units leaving the factory have per-unit I/Q
phase and amplitude calibration data stored in non-volatile memory.
There exist several API functions that read the calibration settings for
a given LO frequency as well as allow a user to either override or
supplement the adjustment values. At this time, if a unit does not have
stored calibration data, there is no default data set. The API functions
that provide access to this feature are as follows. Refer to the API
documentation for additional details on this interface.

-  :code:`skiq_read_iq_complex_multiplier()`
-  :code:`skiq_read_iq_cal_complex_multiplier()`
-  :code:`skiq_read_iq_cal_complex_multiplier_by_LO_freq()`
-  :code:`skiq_write_iq_complex_multiplier_absolute()`
-  :code:`skiq_write_iq_complex_multiplier_user()`
-  :code:`skiq_read_iq_complex_cal_data_present()`

Note that :code:`skiq_read_iq_complex_multiplier()` returns the complex
multiplication factor currently in use, while 
:code:`skiq_read_iq_cal_complex_multiplier()` (note the extra `cal` in
the function name), returns the complex multiplication factor as determined
by the unit’s factory calibration. These two factors may differ if a user
has overwritten (:code:`skiq_write_iq_complex_multiplier_absolute()`) or
supplemented (:code:`skiq_write_iq_complex_multiplier_user()`) the factor
for a given receiver handle.

The factor is automatically updated (or reset) whenever the receive LO
frequency is configured. Any user modifications to the multiplication
factor must be reapplied after a frequency configuration.

In |libsidekiq| v4.7.0, these multipliers are always in effect on Sidekiq
X4. If a user wishes to disable them, setting an absolute factor of 1+0j
is recommended.

Configuring a Tx Interface
==========================

There are various aspects of the transmit interface which must be
configured prior to starting streaming. These control the behavior of
:code:`skiq_transmit()` and are described in detail in the following
sections. For recommendations on how to evaluate the Tx throughput
performance for a particular host system and the various Tx
configuration parameters, refer to :ref:`Transmit Performance`.

.. _block_size_configuration:

Block Size Configuration
------------------------

The block size is the number of samples included in each transmit
packet. A transmit packet consists of the transmit header data as well
as the block, which contains the transmit samples (refer to
:ref:`figure_tx_packet_structure` for the structure of a transmit packet).
While in packed mode, the block size refers to the number of words
contained in the packet not including the header data.

The block size is adjustable and in general, a larger block size results
in higher throughput without underruns or late timestamps. The trade-off
of using a larger block size is an increase in latency in the transfer
of the transmit packet.

A block size + Tx header size (as defined by
:code:`SKIQ_TX_HEADER_SIZE_IN_WORDS`) must be a multiple of 256 words.
The block size can be configured with
:code:`skiq_write_tx_block_size()`. If an invalid block size is
configured, :code:`skiq_start_tx_streaming()` will result in a failure. A
few examples of valid block sizes when running in single channel mode
are: 1020 (packet size=1020+4=1024), 2044 (packet size=2044+4=2048), and
16380 (packet size=16380+4=16384).

When running in dual channel mode, the block size refers to the number
of words contained for each channel. Also, when in dual channel mode,
the maximum block size is limited by the FPGA Tx FIFO size. Note: dual
channel transmit is supported only with Sidekiq m.2, X2, X4, and NV100. Dual channel transmit can be
with A1/A2 or A1/B1 handle pairs.  The secondary handle is used in calls to start streaming and
:code:`skiq_transmit()`.  When
running in dual channel, a few example valid packet sizes are: 1022
(packet size=1022 TxA1 samples + 1022 TxA2 samples + 4 header=2048),
2046 (packet size=2046 TxA1 samples + 2046 TxA2 samples + 4
header=4096), and 8190 (8190 TxA1 samples + 8190 TxA2 samples + 4
header=16384)

When running in packed mode (Sidekiq mPCIe, m.2, and m.2-2280 only),
care must be used to ensure that the block size contains an integer number
of samples and that the block size + header size remains a multiple of 256
words. In packed mode, the block size refers to the number of words contained,
not the number of samples. For example, a block size of 252 results in
336 packed samples (252 \* :code:`SKIQ_PACKED_SAMPLE_RATIO` = 336), which
is a valid packed mode configuration. However, a block size of 508
results in 677.3 samples (508 \* :code:`SKIQ_PACKED_SAMPLE_RATIO` = 677.3),
which is invalid.

Data Flow Mode
--------------

The Tx interface can operate in one of several "data flow modes", as
configured by the user application. The default data flow mode is
"immediate". In this mode, timestamps are ignored and the data is
buffered up and transmitted out as soon as possible. This is useful for
applications that do not have a requirement for precise timing.

For applications that require fine grained control of the timing of the
transmitted samples, the "with\_timestamps" data flow mode should be
utilized. Each block of I/Q samples sent down to the Tx interface will
need to have a 64-bit timestamp located at
:code:`SKIQ_TX_TIMESTAMP_OFFSET_IN_WORDS` into the beginning of the block
of data by the application (see ``tx_samples.c`` in the ``test_apps``
directory for an example). This timestamp corresponds to the time when
the very first sample of the block will be transferred from the FPGA's
I/Q sample FIFO to the D/A converters. Each subsequent complex sample in
the block will then be transferred to the D/A converters one at a time
as the timestamp increments. This continues until the entire data block
has been transmitted out. If continuous transmission of I/Q samples is
required, it is up to the user application to ensure that this pipeline
of data between the user application and the FPGA transmit FIFO is kept
filled, thus preventing an underflow. In this mode, the FPGA will not
transmit any samples that specify a timestamp that is in the past; the
samples will be discarded, the Tx FIFO will be flushed, and the late
timestamp counter is incremented (which can be read with the
:code:`skiq_read_tx_num_late_timestamps()` function call).

As of |libsidekiq| v4.6.0, certain bitstreams support an additional mode:
"with\_timestamps\_allow\_late\_data". Selecting this mode will result
in a return value of :code:`-ENOTSUP` if not supported. In this mode, the FPGA
will transmit samples as described in the above "with\_timestamps"
section, but will also transmit samples that have a timestamp that's
already past. Please note that in this mode, the late timestamp counter
will not be updated, even if samples with late timestamps are
transmitted.

Tx Timestamp Clock Source selection
-----------------------------------

The timestamp source can be configured to either the system or RF (default)
clock, when used in conjunction with a timestamp data flow mode.  Using a system
timestamp may be preferred if an application frequently changes sample rates
(which causes the RF timestamp to pause).  One caveat for using system timestamps
is that it requires a data flow mode of "allow late timestamps mode" due to the
implementation.

Configuring the Tx timestamp clock source is supported on systems using FPGA
bitstream v3.15.1 and above.

Transfer Mode
-------------

The transfer mode of the transmit interface can be configured to operate
either synchronously or asynchronously. The transfer mode can be
configured with the :code:`skiq_write_tx_transfer_mode()` function.

When running in :code:`skiq_tx_transfer_mode_sync` mode,
:code:`skiq_transmit()` blocks if the FPGA transmit FIFO is full until
there is space for the next packet of data. The FPGA FIFO is relatively
small (as defined by :code:`skiq_fpga_tx_fifo_size_t`), so it may be
necessary for users to implement their own external buffering of sample
data prior to calling the :code:`skiq_transmit()` call. This mode can be
simpler to interface with in that once the :code:`skiq_transmit()` function
returns, the data has been transferred to the FPGA and can immediately
be freed or reused by the application. However, it only allows for a
single packet at a time to be in flight to the FPGA. This results in a
potential decrease of throughput efficiency. The ``tx_samples.c``
application provides an example of how to use the synchronous transfer
mode.

An alternative to the synchronous transfer mode is to run in
:code:`skiq_tx_transfer_mode_async`, which results in an increased
throughput. When running in :code:`skiq_tx_transfer_mode_async` mode,
:code:`skiq_transmit()` returns immediately with a status of either :code:`0`
or :code:`SKIQ_TX_ASYNC_SEND_QUEUE_FULL`. A maximum of
:code:`SKIQ_MAX_NUM_TX_QUEUED_PACKETS` can be queued at given point in
time. If a status of :code:`0` is returned, this indicates that the packet was
successfully queued for transmission but not necessarily transferred to
the FPGA yet. A status of :code:`SKIQ_TX_ASYNC_SEND_QUEUE_FULL` indicates
that the software buffer is full and the packet was not queued
successfully. The :code:`skiq_transmit()` call must be repeated with this
data buffer to transmit it. In order for the application to be notified
when the asynchronous transmit operation has been completed by the
Sidekiq library, a function pointer must be registered with the
:code:`skiq_register_tx_complete_callback()` prior to starting streaming
on the Tx interface. Once the buffer has been successfully transferred
to the FPGA, the callback function is called and the status of the
transmission, a pointer to the data of the completed transmission, and
optional user defined data associated specified when initiating the
transmission is provided. At this point, it is safe to either reuse or
free the buffer. Prior to the callback, the buffered should be
considered in use by the Sidekiq library. The ``tx_samples_async.c``
application provides an example of how to use the asynchronous transfer
mode. The :code:`tx_benchmark` application can be used to help assess the
performance tradeoffs with the different transfer modes and block sizes
on the target host platform, as described in :ref:`Receive Performance`.

Starting the Tx Interface
=========================

Once a Tx interface has been properly configured for operation, the
interface can then be started to begin data flowing between the CPU and
FPGA.

.. note:: If the Sidekiq card is configured for dual channel mode, both TxA1
   and TxA2 interfaces are enabled when :code:`skiq_start_tx_streaming()` is
   called with :code:`skiq_tx_hdl_A2` as the :code:`hdl` argument.  As of
   |libsidekiq| v4.13.0 and FPGA bitstream v3.13.0, the Sidekiq X4 can also
   transmit using TxA1 and TxB1 to provide the user two independently tunable
   interfaces.  At its introduction in |libsidekiq| v4.17.0, Sidekiq NV100
   also supports transmit using TxA1 and TxB1.  In that case, the
   :code:`skiq_tx_hdl_B1` handle needs to be specified as the :code:`hdl`
   argument to :code:`skiq_start_tx_streaming()`.

.. caution:: In a dual channel mode transmit configuration, the user must
   **only** specify the second handle to :code:`skiq_start_tx_streaming()` and
   :code:`skiq_stop_tx_streaming()` variants.  Both Tx interfaces are enabled
   within each function call whenever the second handle is specified.

An update to the data flow mode / transfer mode / block size is allowed
at any time, but is only applied when starting the Tx interface. An
example of configuring and starting the interface is shown below.

.. code-block:: c

  /* configure the data flow mode to use timestamps */
  skiq_write_tx_data_flow_mode(card, skiq_tx_hdl_A1,
                              skiq_tx_with_timestamps_data_flow_mode);

  /* configure the transfer mode to synchronous */
  skiq_write_tx_transfer_mode(card, skiq_tx_hdl_A1,
                              skiq_tx_transfer_mode_sync);

  /* configure the size of the block to send */
  skiq_write_tx_block_size(card, skiq_tx_hdl_A1, num_samples);

  /* begin streaming on the Tx A1 interface */
  skiq_start_tx_streaming(card, skiq_tx_hdl_A1);

Writing I/Q Samples to a Tx Interface 
--------------------------------------

Once the Tx interface has been started, it is up to the user application
to provide the I/Q data to |libsidekiq| for transmission. The I/Q data is
provided to |libsidekiq| by calling :code:`skiq_transmit()`. The
:code:`skiq_transmit()` can block if running in the synchronous transfer
mode or return immediately if running the asynchronous transfer mode.
For details on the transfer mode, refer to :ref:`Transfer Mode`.

The user application is responsible for populating the sample data prior
to calling :code:`skiq_transmit()`. The format of each user-provided
transmit packet is shown in :ref:`figure_tx_packet_structure`.
The :code:`skiq_tx_block_t` struct type definition has fields that allow
for easy access to the header and I/Q data. The :code:`miscHigh` and
:code:`miscLow` fields are reserved for future use. The :code:`timestamp` field
is reserved for the transmit timestamp. In the "with\_timestamps" data
flow mode, the :code:`timestamp` field is used to determine when the FPGA
will actually send the data to the D/As. If the FPGA detects that the
timestamp specified by software has already passed, the entire FPGA FIFO
will be flushed and an error count is incremented. The number of
"late timestamps" detected by the FPGA can be queried with the
:code:`skiq_read_tx_num_late_timestamps()` API. On certain bitstreams,
the "with\_timestamps\_allow\_late" data flow is available that acts
much like "with\_timestamps" mode, though the FPGA will transmit any
samples with a timestamp that has already passed and not increment the
"late timestamp" count. The API function
:code:`skiq_read_last_tx_timestamp()` provides the caller with the last
timestamp the FPGA is acting upon and is useful for debugging
applications.

In the "immediate" data flow mode, this timestamp value is ignored and
can be set to zero by user applications. It is the user's responsibility
to ensure that the FPGA has enough data to transmit. If the FPGA
encounters a case where its FIFO is empty and there is no data to
transmit while streaming, it will increment an underrun error count.
This count can be queried with the :code:`skiq_read_tx_num_underruns()`
API.

The sample data (the :code:`data` field of the :code:`skiq_tx_block_t` struct)
are 32-bit values, where the most significant 16-bits contains the "I"
data, and the least significant 16-bits contain "Q" data. Each "I" and
"Q" data word is represented in little endian form as a twos-complement
number, sign-extended from the actual width of the D/A converters used
in Sidekiq to 16-bits. For the Sidekiq mPCI, m.2, Z2, m.2-2280 and Matchstiq Z3u
there are 12 data bits in each sample; for both Sidekiq X2 and Sidekiq X4, there
are 14 data bits in each sample; and for Sidekiq NV100, there are 16 data bits
in each sample.

The number of samples contained within a single block of data is
variable and is configured with the :code:`skiq_write_tx_block_size()`
API, as described in :ref:`block_size_configuration`. The format of the
structure of the transmit packet is shown in :ref:`figure_tx_packet_structure`.

.. _figure_tx_packet_structure:
.. figure:: _static/tx_packet_structure.png
   :alt: Tx Packet Structure
   :align: center
   :width: 6.92520in
   :height: 5.21180in

   Tx Packet Structure

See the ``tx_samples.c`` and ``tx_samples_async.c`` example applications
in the ``test_apps/`` directory for details of how a real-world
application uses this interface in both synchronous and asynchronous
transfer modes.

Simultaneous use of Tx and Rx Interfaces
========================================

Most Sidekiq products support running the Tx and Rx interfaces
simultaneously, with the exception of Sidekiq X4; these products support
both FDD and TDD applications. Specifically, for an FDD application, the
Rx interface can be tuned to one RF frequency, and the Tx interface is
tuned to a different RF frequency.  Note that Sidekiq X4 does allow for
a A* handle to be configured for TX simultaneously to a B* handle configured
for RX.

From a software application perspective, a multi-threading library
(such as pthreads) can be used to manage the Rx/Tx interfaces in separate
threads. See the ``fdd_rx_tx_samples.c`` test application for an example
of performing simultaneous FDD operation.

In the case of TDD operation, the Tx and Rx interfaces can be configured
separately, and then proceed through a sequence of starting and stopping
the Tx and Rx interfaces as each transmit or receive operation is
performed. An example of typical TDD operation can be found in the
``tdd_rx_tx_samples.c`` test application. Note: the transmit and
receive FIFOs are flushed upon restarting the interface, so if the
interface is stopped prior to the sample data completing reception or
transmission, the data will be flushed.

Both the Rx and Tx interfaces share a common sample rate clock and
timestamp, with the exception of certain configurations of the Sidekiq
X2 resulting in a configuration where the Tx interface sample rate is
twice the Rx sample rate. As a result, it is only necessary to configure
the sample rate and reset the timestamp (if desired) for either the Rx
or Tx interface.

Stopping and Releasing an Interface
===================================

When an application no longer needs to transfer data with a previously
started interface, the interface can be stopped which will prevent
future data transfers until the next "start" function is executed.
Stopping an Rx interface takes place when the
:code:`skiq_stop_rx_streaming()` function is called. Stopping a Tx
interface takes place when the :code:`skiq_stop_tx_streaming()` function
is called. Both the system timestamp and RF pair timestamp continues to
increment regardless of whether the system is currently streaming.

Starting and Stopping on 1PPS
-----------------------------

Sidekiq can be configured to start and stop streaming on a future 1PPS
edge. To start Rx streaming on a 1PPS edge, the function
:code:`skiq_start_rx_streaming_on_1pps()` is called by the user
application. To stop Rx streaming on a 1PPS edge, the function
:code:`skiq_stop_rx_streaming_on_1pps()` is called by the user
application. A similar function exists for controlling the streaming
operation on Tx (i.e., :code:`skiq_start_tx_streaming_on_1pps()` and
:code:`skiq_stop_tx_streaming_on_1pps()`). All of the
:code:`skiq_*_streaming_on_1pps()` functions block until the 1PPS occurs.

To ensure that data begins to flow when the 1PPS occurs without any
dropout, it is highly recommended that one thread is used to
receive/transmit the data, and a separate thread is used to call the
:code:`skiq_start_*_streaming_on_1pps` function. Additionally, the thread
performing the receiving/transmitting of data should be started prior to
the call to start streaming. To ensure that the first packet queued to
transmit is the first desired samples, it is recommended to register a
callback for when the FPGA is ready to accept samples prior to the 1PPS
occurring. The callback can be registered via the
:code:`skiq_register_tx_enabled()` API.

In the case of stopping the Rx streaming on a 1PPS, the
:code:`skiq_receive()` function needs to continue to be called after
:code:`skiq_stop_rx_streaming_on_1pps()` returns. This stop streaming
function stops the data from being generated by the FPGA. However, there
will be data remaining in the internal FIFOs, so :code:`skiq_receive()`
should be called until no data remains. Once there is no data returned
from the :code:`skiq_receive()` call, the :code:`skiq_stop_rx_streaming()`
function should be called to finalize the disabling of the data flow.

1PPS Source
^^^^^^^^^^^

As of |libsidekiq| v4.7.0, certain Sidekiq products and revisions allow
for configuration of the 1PPS source. Specifically, the source of the
1PPS can be configured to be detected from an alternate pin. This
configuration can be updated while running with the
:code:`skiq_write_1pps_source()` API. Additionally, the current
configuration can be queried with the :code:`skiq_read_1pps_source()`
API.

Working with multiple receive handles
-------------------------------------

As of |libsidekiq| v4.6.0, the following API functions are available to
synchronize or coordinate the starting and stopping of receive
streaming. Each of these functions accepts an array of receive handles
to start / stop together. It is important to understand that the
"multi\_immediate" variant does NOT currently synchronize the receive
handles, but does offer a convenient way to start and/or stop multiple handles in
a single call. As of |libsidekiq| v4.9.0, the "synchronized trigger"
source was introduced that allows for multiple receive handles on a
given card to start and/or stop with their RF timestamps synchronized.

Refer to the Sidekiq API documentation for more details on these
functions.

-  :code:`skiq_start_rx_streaming_multi_immediate()`
-  :code:`skiq_start_rx_streaming_multi_on_trigger()`
-  :code:`skiq_stop_rx_streaming_multi_immediate()`
-  :code:`skiq_stop_rx_streaming_multi_on_trigger()`

For detailed examples on starting/stopping on a 1PPS (receive and/or
transmit) or a synchronized trigger (receive only), refer to the test
applications ``rx_samples_on_trigger.c`` and ``tx_samples_on_1pps.c``.

Pin Control enable of RFIC signal paths (Sidekiq X4 only)
=========================================================
Since |libsidekiq| v4.14.0, the Tx/Rx signal paths on X4 can be controlled
either through the libsidekiq API or by pins asserted by the FPGA user_app.
For minimal latency when switching between receive and transmit, pin control
is recommended.

Please consult the Sidekiq API documentation for more details on these
functions.

-  :code:`skiq_read_rx_rfic_pin_ctrl_mode()`
-  :code:`skiq_read_tx_rfic_pin_ctrl_mode()`
-  :code:`skiq_write_rx_rfic_pin_ctrl_mode()`
-  :code:`skiq_write_tx_rfic_pin_ctrl_mode()`

Clock and Time Management Resources
===================================

Sidekiq uses a common reference clock to drive all of the RF/baseband
hardware. This reference clock can come from other an on-board
temperature compensated voltage controlled oscillator (TCVCXO) or an
external reference clock (refer to Hardware User’s Manual for specific
Sidekiq type). By default, Sidekiq uses the on-board TCVCXO for its
reference clock. This on-board TCVCXO has a stability of ±1 PPM over
the temperature range from -30 deg C to +85 deg C. For applications that
need to dial in the accuracy of this timing reference even further,
|libsidekiq| provides an API call to warp the timing reference by
generating an analog control voltage using an on-board D/A converter
(DAC)  dedicated to this purpose. The :code:`skiq_write_tcvcxo_warp_voltage()`
function is used to warp the control voltage of the oscillator. The
reference clock can be adjusted by -1 to +6 ppm by adjusting the DAC
voltage. Valid warp voltage ranges are 0.75-2.25V, which corresponds to
DAC values between 0 and 1023 for Sidekiq mPCIe / Sidekiq m.2 / Sidekiq
Z2. On Sidekiq X2 rev C (unsupported in rev B), Sidekiq X4, Sidekiq Stretch,
and Matchstiq Z3u the warp voltage ranges are 0.4-2.4V, or values of 7944-47662. For
details on providing an external reference clock, refer to :ref:`[6]
<ref6>`.

In revision C of the mPCIe hardware and all revisions of the m.2, X2, and
X4 hardware, the reference clock source is configurable via software.
The configuration of the reference clock is a persistent setting. The
current source can be queried via the
:code:`skiq_read_ref_clock_select()` API. As of |libsidekiq| v4.7.0, an
additional reference clock source of a host, :code:`skiq_ref_clock_host`,
can also be configured for specific Sidekiq products. The host reference
clock configuration utilizes an alternate clock frequency and input. For
details on how to update this configuration, contact Epiq Solutions 
:ref:`[5] <ref5>`.

The reference clock source for a card can be temporarily changed on-the-fly,
allowing applications to choose the clock source as needed. The source can
be changed via the :code:`skiq_write_ref_clock_select()` function. This
function will update the current source, however this change will not be
stored in memory nor maintained between applications. This is not
supported for all Sidekiq products. Products that do not support changing
the reference clock source are Sidekiq M.2 and Sidekiq mPCIe cards.

The reference clock frequency for a card can also be temporarily changed on-the-fly,
allowing applications to switch between external reference frequencies as needed.
The frequency can be changed via the :code:`skiq_write_ext_ref_clock_freq()` function
and must be a supported external reference clock frequency per the card specification.
This function will update the expected clock frequency, however this configuration is
runtime only and is not stored on the card nor permanent.  Please note, this function
will also automatically update the reference clock selection to an external reference
clock source.  The reference clock selection is also not stored on the card nor
permanent.  Changing the reference clock frequency using :code:`skiq_write_ext_ref_clock_freq()`
will stop any ongoing receiving or transmitting.  Runtime reference clock frequency
switching is only supported on Sidekiq Stretch and Sidekiq NV100 as of |libsidekiq|
v4.17.0.

GPSDO
-----

As of |libsidekiq| v4.15.0, the GPS Disciplined Oscillator (GPSDO) is available on
Sidekiq Stretch when using FPGA bitstream v3.14.1 or later.  GPSDO is available
for Matchstiq Z3u as of |libsidekiq| v4.16.0.  GPSDO is available
for Sidekiq NV100 as of |libsidekiq| v4.17.0. Its functionality can be enabled
with :code:`skiq_gpsdo_enable()` (see :ref:`sidekiq_api` for API functions related
to the GPSDO).  When a GPS fix has been obtained by the Sidekiq's on-board GPS,
the FPGA uses the GPS unit's 1PPS signal to increase the accuracy of the Sidekiq's
on-board oscillator by automatically adjusting the DAC warp voltage. If no GPS fix
can be obtained or is lost, the DAC warp voltage is kept at its current value; if
no GPS fix is available on startup, the warp voltage is kept at its factory
calibrated default value. As the FPGA is now in control of the warp voltage, this
prevents its manual adjustment through the
:code:`skiq_write_tcvcxo_warp_voltage()` API function.  Additionally, there are
some sensor peripherals that share a bus with the DAC warp voltage.  As such,
access to those sensors through API functions may, in some cases, indicate that
data is not available (:code:`-EAGAIN`). For example, in the case of Sidekiq
Stretch, the temperature sensor measurement may not be available for up to one
second after a call to :code:`skiq_gpsdo_enable()` and a call to
:code:`skiq_read_temp()` will return :code:`-EAGAIN` if called in that time
period. The :code:`skiq_gpsdo_is_locked()` function (available as of |libsidekiq|
v4.17.0) queries the GPSDO control algorithm on the FPGA to check for a lock
between the 1PPS signal and the disciplined oscillator.

Starting with FPGA bitstream v3.15.1, the GPSDO algorithm can use one
of a few different 1PPS sources for disciplining for on-board reference clock.
The GPSDO 1PPS source configuration matches the card's 1PPS source configuration
and can be accessed or modified by calling :code:`skiq_read_1pps_source()` or
:code:`skiq_write_1pps_source()` respectively.  Please note that when
:code:`skiq_1pps_source_host` is selected, the GPSDO algorithm only uses the
1PPS when the GPS module additionally indicates a timing fix.  If
:code:`skiq_1pps_source_external` is selected as the 1PPS source, then GPSDO
algorithm uses the 1PPS unconditionally.

.. note:: The GPSDO algorithm is unique since its execution may persist after a
          |libsidekiq| application exits.  Support for persistent execution was
          added to the Matchstiq Z3u and Sidekiq Stretch and Sidekiq NV100 as 
          of |libsidekiq| v4.17.5. The algorithm resides in the FPGA and does 
          not require software intervention to continue working.  The side
          effect to this behavior is that previous |libsidekiq| releases (prior
          to v4.15.0) will not be able to read or write the DAC warp voltage
          nor, in the case of Sidekiq Stretch, read the temperature sensor
          measurements at all.  However, applications linked against
          |libsidekiq| v4.15.0 or later will have alternate access to the DAC
          warp voltage and temperature sensors as described above.

Timestamp Details
=================

There is both an RF timestamp and System timestamp maintained by the
Sidekiq card. The RF timestamp for both receive and transmit are
identical and increment at the rate of the sample rate. For each tick of
the sample rate clock, the RF timestamp increments by one. For Sidekiq
mPCIe, m.2, m.2-2280, Z2, NV100, and Matchstiq Z3u, the System timestamp
increments independent of the sample rate. For Sidekiq X2 and Sidekiq
X4, the System timestamp increments at a rate relative to the sample rate
and continues to increment regardless of any radio configuration changes
(with the exception of sample rate). The System timestamp frequency can be
queried with the :code:`skiq_read_parameters()` function, and the current
frequency value is located in :code:`sys_timestamp_freq` variable of the
:code:`skiq_fpga_param_t` data structure.

The timestamps can be reset to zero asynchronously via the
:code:`skiq_reset_timestamp()` function. This will reset both the RF and
System timestamps. Additionally, if it is desired to set the timestamps
to a specific value, the :code:`skiq_update_timestamps()` function can be
used. Finally, if it is desired to reset or update the timestamps on a
1PPS edge, then the :code:`skiq_write_timestamp_reset_on_1pps()` or
:code:`skiq_write_timestamp_update_on_1pps()` functions can be used.
This is useful if it is necessary for the application to have the
timestamps synchronized to a 1PPS source.

Automatic Calibration
=====================

Automatic calibration may be enabled and performed by Sidekiq by
default. Automatic calibration algorithms include DC offset reduction as
well as quadrature error correction for both receive and transmit.
Depending on the processing being performed by the user radio
application, automatic calibration may not be desired.

As of |libsidekiq| v4.6.0, the ability to disable automatic transmit
quadrature calibration algorithm is supported. Additionally, the ability
to manually run the Tx quadrature calibration algorithm is supported.
The Tx quadrature algorithm may take time to converge and may not be
desired to leave in automatic mode. Additionally, execution of the Tx
quadrature algorithm results in the appearance of erroneous
transmissions in the spectrum while running. To configure the Tx
quadrature calibration algorithm mode to run either manually or
automatically, the :code:`skiq_write_tx_quadcal_mode()` API can be used.
The currently configured mode can be queried with the
:code:`skiq_read_tx_quadcal_mode()`. To manually initiate the
calibration algorithm to run, the :code:`skiq_run_tx_quadcal()` API can
be used. Refer to the ``tx_samples.c`` application for the APIs
associated with this.

As of |libsidekiq| v4.13.0, the ability to disable automatic receive
calibration algorithms is supported with :code:`skiq_write_rx_cal_mode()`.
Additionally, the specific calibration types ran can be configured with
:code:`skiq_write_rx_cal_type_mask()`.  The available calibrations that
can be enabled can be queried with :code:`skiq_read_rx_cal_types_avail()`.
Finally, if it is desired to manually run the RX calibration, the
:code:`skiq_run_rx_cal()` API can be used.  Refer to the ``rx_samples.c``
application for an example use of these APIs.

Receive Stream Mode
===================

A typical use case for the Sidekiq line of products is to receive a
great deal of I/Q data as efficiently as possible. This high throughput
use case would historically receive 4,096 bytes per block.

As of |libsidekiq| v4.6.0, the low latency receive stream mode
(:code:`skiq_rx_stream_mode_low_latency`) provides a smaller block of I/Q
samples from :code:`skiq_receive()` more often and effectively lowers the
latency from RF reception to host CPU. This is especially useful when
using lower sample rates that would historically take a relative long
time to fill up a 4kB I/Q block before delivering the samples to the
host CPU and software application.

As of |libsidekiq| v4.7.0, the balanced stream mode
(:code:`skiq_rx_stream_mode_balanced`) is also available. The balanced
stream mode is a compromise between the high throughput and low latency
stream modes. It results in a reduced throughput relative to the
optimized high throughput mode, but produces a larger number of samples
per packet than the low latency mode, thus achieving a higher throughput
than the low latency mode. Most applications interested in achieving the
maximum throughput should use the default high throughput mode, which
applications interested in having a minimal latency should use the low
latency mode. The balanced mode offers a compromise between the low
latency and high throughput options.

Refer to the Sidekiq API for additional details on the receive stream
modes available. The API type and functions follow:

-  Type: :code:`skiq_rx_stream_mode_t`
-  Function: :code:`skiq_read_rx_stream_mode()`
-  Function: :code:`skiq_write_rx_stream_mode()`
-  Function: :code:`skiq_read_rx_block_size()`

Hotplug
=======

As of |libsidekiq| v4.14.0, card hotplugging is now supported. Hotplug
support will allow the user to connect and remove cards, both physically
and logically, during an application's execution. Removing a card that is
in use by the application will result in adverse outcomes. New cards will
be listed in calls to the :code:`skiq_get_cards()` function. Cards can then
be initialized using :code:`skiq_enable_cards()`.

Exiting
=======

When an application is ready to exit, the :code:`skiq_exit()` function
should be called. This function ensures that |libsidekiq| shuts down
gracefully. Any |libsidekiq| function should not be called within a signal
handler, as there are various mutexes utilized to control access to
|libsidekiq| and these mutexes may already be locked prior to being
attempted to be called from within the context of the signal handler,
which may result in deadlock. It is instead recommended to clear a
"running" flag within the signal handler and perform the appropriate
Sidekiq shutdown within the context of the main application. Refer to
the ``tx_samples.c`` test application for an example of this.

Once :code:`skiq_exit()` has been called, all follow-up calls to |libsidekiq|
will fail with the exception of :code:`skiq_init()` or
:code:`skiq_get_cards()`. Calling :code:`skiq_init()` will re-initialize the
library and prepare it for usage by a host application.

As of |libsidekiq| v4.14.0, :code:`skiq_exit()` is called automatically when the
application using the library shuts down; this is meant as a safety precaution
to ensure that |libsidekiq| is properly cleaned up. If for some reason this
is not desired, the exit handler can be disabled using the
:code:`skiq_set_exit_handler_state()` function.  This function must be called
before |libsidekiq| is initialized (for example, through :code:`skiq_init()`).
Despite the presence of this exit handler, :code:`skiq_exit()` should still be
explicitly called in |libsidekiq| applications to ensure that initialized
radios (and the library) are cleaned up and powered down when no longer needed.


Critical Errors
===============

It is possible that within |libsidekiq|, a critical error may be
encountered. Once a critical error condition is detected in |libsidekiq|,
it is no longer safe to continue accessing |libsidekiq|. The application
should be shutdown and the cause of the error should be resolved.
Continued operation on the Sidekiq may result in unpredictable /
incorrect behavior. The default behavior of |libsidekiq| when encountering
a critical error is to exit. If an application wishes to override the
default behavior, a callback function can be registered via the
:code:`skiq_register_critical_error_callback()` API. The function
registered is then called if |libsidekiq| encounters a critical error.
Note that continued access of |libsidekiq| after a critical error has
occurred can result in undefined behavior and should not be done.

