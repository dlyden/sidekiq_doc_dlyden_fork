.. raw:: latex

    \begin{landscape}

Sidekiq X4 Block Diagram
========================

.. figure:: _static/sidekiq_x4_architecture_for_sdk_v2.png
   :alt: Sidekiq X4 block diagram showing how libsidekiq + user applications fit in the system
   :align: center

   Sidekiq X4 block diagram showing how libsidekiq + user applications fit in the system

.. raw:: latex

    \end{landscape}
