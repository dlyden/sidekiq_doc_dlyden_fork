.. include:: replacements.rst

.. _section_skiq_remote:

Using Libsidekiq Remotely
=========================
Introduced in |libsidekiq| v4.16.0 is a mechanism for running |libsidekiq| applications on a client 
which connects to a server supported by the network transport interface.  Both the Sidekiq Z2 
and Matchstiq Z3u support this functionality. The server physically contains the Sidekiq card.  
An overview of the deployment of such a system is depicted below.

.. figure:: _static/skiq_remote.png
   :alt: Sidekiq Remote Deployment
   :align: center
   :width: 4.562in
   :height: 5.011in

   Sidekiq Remote Deployment
  
The server containing the Sidekiq card allows for applications to be run either remotely
or locally on the server itself.  Management of the Sidekiq card resource is handled by 
the daemon.  Client(s) communicate with a server via a network interface, using a daemon
API to send and receive messages.

Prerequisites
-------------

netconfig
^^^^^^^^^
After the integration of libtirpc into |libsidekiq| v4.17.5, network transport now requires
a ``netconfig`` file be present on the host. A default version of the contents of the ``netconfig``
file are provided below if the host operating system does not include it.  Since each
operating system may handle this file differently, please contact Epiq Solutions for
installation instructions if needed.

.. code-block:: none
    # The network configuration file. This file is currently only used in
    # conjunction with the TI-RPC code in the libtirpc library.
    #
    # Entries consist of:
    #
    #       <network_id> <semantics> <flags> <protofamily> <protoname> \
    #               <device> <nametoaddr_libs>
    #
    # The <device> and <nametoaddr_libs> fields are always empty in this
    # implementation.
    #
    udp        tpi_clts      v     inet     udp     -       -
    tcp        tpi_cots_ord  v     inet     tcp     -       -
    udp6       tpi_clts      v     inet6    udp     -       -
    tcp6       tpi_cots_ord  v     inet6    tcp     -       -
    rawip      tpi_raw       -     inet      -      -       -
    local      tpi_cots_ord  -     loopback  -      -       -
    unix       tpi_cots_ord  -     loopback  -      -       -

.. note:: In a future release of |libsidekiq| a patch will be released to remove
    this requirement for using network transport.

Matchstiq Z3u 
^^^^^^^^^^^^^
The *z3u-usb-net-* package must be at least at least version *1.0.0-1*.  To view the currently 
installed *z3u-usb-net* package installed on the Z3u, run :code:`apt list z3u-usb-net`.

In order to decrease the overall system latency and improve the streaming throughput, it is highly 
recommended to use the Ethernet Emulation Model (EEM) USB networking protocol.  To configure the 
USB network to use this profile, run :code:`sudo fw_setenv usb_network_profile eem` on the Z3u 
(refer to the "USB Network Configuration" section of the Matchstiq Z3u Hardware User Manual).  
Note that after updating the profile, the system must be rebooted for the settings to be applied.

Sidekiq Z2 
^^^^^^^^^^
A minimum BSP of v3.3.0 is required.


Client 
^^^^^^
Libsidekiq utilizes Open Network Computing (ONC) Remote Procedure Calls (RPC) to provide acceleration 
of a handful of |libsidekiq| functions.  As a result, the client system most have RPC bind installed.  
For instance, on a Debian machine (such as Ubuntu 18.04), you can run :code:`sudo apt install rpcbind`.

Only Linux on a x86-64 bit processor is currently supported.  For support of alternative operating system 
support, please contact Epiq Solutions.

Setup
-----

Client System
^^^^^^^^^^^^^
Connecting to a remote Sidekiq card requires setting environment variables **SKIQ_DAEMON_IP** and
**SKIQ_DAEMON_PORT** with the server's IP address and port (default port of 3417).  

Server System
^^^^^^^^^^^^^
The server must be running the daemon process (*skiq_daemon*) prior to accepting client connections. 
The *skiq_daemon* test application is provided for both the Sidekiq Z2 and the Matchstiq Z3u 
in the prebuilt applications bundled with a Sidekiq release.

Matchstiq Z3u
^^^^^^^^^^^^^
For the Matchstiq Z3u, a Debian package, *z3u-skiq-daemon* is provided.  This package includes 
the *skiq_daemon* application as well as a service that is enabled to run automatically upon 
startup once the package is installed.  Note that the card ownership is managed by *skiq_daemon* 
such that running *skiq_daemon* will not prevent applications from running natively on the Matchstiq 
Z3u if client does not have the Matchstiq Z3u card enabled.  If it is desired to disable 
*skiq_daemon* from running automatically upon startup, this can be disabled by running 
:code:`sudo systemctl disable z3u-skiq-daemon` on the Z3u.

Example Usage
-------------
Let's consider an example deployment of running a Matchstiq Z3u connected to a Linux laptop.  
The Linux laptop will be running an x86-64 bit version of |libsidekiq|.


.. figure:: _static/skiq_remote_example.png
   :align: center

   Sidekiq Remote Example Usage


Since the Matchstiq Z3u automatically starts *skiq_daemon* when powered on, there is no additional
steps required to setup the Matchstiq Z3u.

On the Linux laptop, we must configure the daemon IP address and port number.  By default, 
the Matchstiq Z3u's IP address is 192.168.0.15.  Additionally, the default port number of *skiq_daemon*
is 3417.  To configure the IP address and port number, on the laptop, we can run:: 

    $ export SKIQ_DAEMON_IP=192.168.0.15
    $ export SKIQ_DAEMON_PORT=3417
 

Now, also on the Linux laptop, we can run the *version_test* application. ::

    $ ./version_test
    1 card(s) found: 0 in use, 1 available!
    Card IDs currently used     : 
    Card IDs currently available: 0 
    Info: initializing 1 card(s)...
    SKIQ[18197]: <INFO> libsidekiq v4.16.0 (gXXXXXXXXX)
    version_test[18197]: <INFO> Sidekiq card 0 is serial number=9X0J, Z3U (rev B) (part ES032201-B0-00)
    version_test[18197]: <DEBUG> got port 3417, ip 192.168.0.15 card=0
    version_test[18197]: <DEBUG> Allocating new hash table
    version_test[18197]: <DEBUG> Allocating new connection 5620507011113330880
    version_test[18197]: <DEBUG> Creating new RPC client
    version_test[18197]: <DEBUG> Saving server card 0 (uid=5620507011113330880) to client card 0
    version_test[18197]: <WARNING> FPGA capabilities indicate no support for reading/writing flash for card 0
    version_test[18197]: <INFO> Sidekiq card 0 FPGA v3.14.0, (date 20081217, FIFO size unknown)
    version_test[18197]: <INFO> Sidekiq card 0 is configured for an internal reference clock
    ***********************************************************
    * libsidekiq v4.16.0
    ***********************************************************
    ***********************************************************
    * Sidekiq Card 0
    Card
        accelerometer present: true
        part type: Z3U
        part info: ES032201-B0-00
        serial: 9X0J
        xport: network
    FPGA
        version: 3.14.0
        git hash: 0x1eefb308
        build date (yymmddhh): 20081217
        tx fifo size: unknown
    RF
        reference clock: internal
        reference clock frequency: 40000000 Hz

    version_test[18197]: <INFO> Unlocking card 0
    version_test[18197]: <DEBUG> Hash table empty, destroying...
 
We can see that our remotely connected Matchstiq Z3u was detected by the *version_test* application 
running on the Linux x86 host.

Detailed Network and Port Use
-----------------------------
The *skiq_daemon* application uses both TCP and UDP sockets for use.  The probe socket exists on 
port number 3417 and can be overwritten by launching the *skiq_daemon* with a different port.  
For example, if the probe port number is desired to be 4000, you can start the *skiq_daemon* 
application with: :code:`./skiq_daemon -p 4000`.  The client will need to specify the 
daemon port as **SKIQ_DAEMON_PORT** as 4000 (:code:`export SKIQ_DAEMON_PORT=4000`).  When a card is in use
by |libsidekiq|, a TCP socket connection is established between the client and server.  The port
number used can vary and typically begins from the **SKIQ_DAEMON_PORT**.  The port in use for 
the control is established during the probe procedure of the card.  Additionally, a TCP and 
UDP socket connection are established to support RPC.  The port number in use to support RPC varies.  
Finally, when streaming samples, a UDP socket is used to stream the sample data.  The port number used
in streaming can vary and typically begins from the **SKIQ_DAEMON_PORT**.

Limited Capabilities
--------------------
The following features / capabilities are not supported in |libsidekiq| v4.17.4 via remote operation using the network transport:

* reprogramming the RFIC via `skiq_prog_rfic_from_file()`
* reprogramming the FPGA via `skiq_prog_fpga_from_file()`
* accessing the flash via 
    * `skiq_verify_fpga_config_from_flash()`
    * `skiq_save_fpga_config_to_flash_slot()`
    * `skiq_verify_fpga_config_in_flash_slot()`
* frequency hopping via 
    * `skiq_write_rx_freq_tune_mode()`
    * `skiq_read_rx_freq_tune_mode()`
    * `skiq_write_tx_freq_tune_mode()`
    * `skiq_read_tx_freq_tune_mode()`
    * `skiq_write_rx_freq_hop_list()`
    * `skiq_read_rx_freq_hop_list()`
    * `skiq_write_tx_freq_hop_list()`
    * `skiq_read_tx_freq_hop_list()`
    * `skiq_write_next_rx_freq_hop()`
    * `skiq_write_next_tx_freq_hop()`
    * `skiq_perform_rx_freq_hop()`
    * `skiq_perform_tx_freq_hop()`
    * `skiq_read_curr_rx_freq_hop()`
    * `skiq_read_curr_tx_freq_hop()`
    * `skiq_read_next_rx_freq_hop()`
    * `skiq_read_next_tx_freq_hop()`
* RX streaming on 1PPS or a trigger source via 
    * `skiq_start_rx_streaming_on_1pps()`
    * `skiq_start_rx_streaming_multi_on_trigger()`
    * `skiq_stop_rx_streaming_on_1pps()`
* RX stream configuration via
    * `skiq_set_rx_transfer_timeout()`
    * `skiq_get_rx_transfer_timeout()`
    * `skiq_write_rx_stream_mode()`
    * `skiq_read_rx_stream_mode()`
* Only a single remote card can be used
* Only a single handle can be used
* RF port selection is not supported on Z2

