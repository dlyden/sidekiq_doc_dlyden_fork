.. _example_x4_use_cases:

Example X4 Use Cases: Rx
========================

This section outlines common use cases possible with Sidekiq X4, demonstrating how
they can be achieved with the supplied test applications.  For users developing an
application from scratch, the equivalent sequence of API calls is provided.  See 
the :code:`rx_samples_minimal.c` and :code:`rx_samples.c` sample applications in 
the :code:`test_apps/` directory for example usage.

.. caution:: Receiving at high sample rates can be RAM & disk intensive.  Performance is dependent on the host platform.

Receive: single channel, up to 200MHz IBW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:code:`test_apps/rx_samples_minimal -c 0 -d /tmp/iq_output -f 850000000 -r 245760000 -b 200000000 --handle=A1`

* where handle can be any of the following: :code:`A1,A2,B1,B2,C1,D1`

.. note:: libsidekiq will use the provided sample rate and bandwidth arguments to select the RFIC profile best
     meeting the constraints.  If the selected rate/bandwidth is not an exact match, it may be desirable to
     define and import a custom profile.  Please see the :ref:`Runtime Loaded User Generated Profiles` section 
     for more details.

The same can be achieved using the following sequence of API functions:

.. code-block:: none

   skiq_init()
   skiq_write_rx_sample_rate_and_bandwidth(card, handle, rate, bandwidth)
   skiq_write_rx_LO_freq(card, handle, frequency)
   skiq_start_rx_streaming_multi_immediate()
   while(receiving)
      skiq_receive(card, handle, rx_block, len)
   skiq_stop_rx_streaming_multi_immediate()
   skiq_exit()

Receive: single channel, up to 400MHz IBW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:code:`test_apps/rx_samples_minimal -c 0 -d /tmp/iq_output -f 850000000 -r 491520000 -b 400000000 --handle=C1`

* where handle can be any of the following: :code:`C1,D1`

The same can be achieved using the following sequence of API functions:

.. code-block:: none

   skiq_init()
   skiq_write_rx_sample_rate_and_bandwidth(card, handle, rate, bandwidth)
   skiq_write_rx_LO_freq(card, handle, frequency)
   skiq_start_rx_streaming_multi_immediate()
   while(receiving)
       skiq_receive(card, handle, rx_block, len)
   skiq_stop_rx_streaming_multi_immediate()
   skiq_exit()

Receive: Two phase coherent channels up to 200MHz IBW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:code:`test_apps/rx_samples_minimal -c 0 -d /tmp/iq_output -f 850000000,850000000 -r 250000000,250000000 -b 200000000,200000000 --handle=A1,A2`

* where handle can be any two of the following: :code:`A1,A2,B1,B2`

The same can be achieved using the following sequence of API functions:

.. code-block:: none

   skiq_init()
   skiq_write_rx_sample_rate_and_bandwidth_multi(card, handles, nr_handles, rates, bandwidths)
   skiq_write_rx_LO_freq(card, handle #1)
   skiq_write_rx_LO_freq(card, handle #2)
   skiq_start_rx_streaming_multi_immediate()
   while(receiving)
      skiq_receive(card, handle, rx_block, len)
   skiq_stop_rx_streaming_multi_immediate()
   skiq_exit()

.. caution:: Phase coherent receive cannot be used in the conjunction with the decimator.
   A warning will be issued when this condition is encountered, but please be aware of the
   limitation.

Receive: Two independently tunable channels different sample rate* up to 200MHz IBW
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

:code:`test_apps/rx_samples_minimal -c 0 -d /tmp/iq_output -f 750000000,850000000 -r 250000000,250000000 -b 61440000,122880000 --handle=A1,B2`

* where handle can be two of the following: :code:`A1,A2,B1,B2`, with the constraint that one handle must be from the "A" group and one from "B".

The same can be achieved using the following sequence of API functions:

.. code-block:: none

   skiq_init
   skiq_write_rx_sample_rate_and_bandwidth_multi(card, handles, nr_handles, rates, bandwidths)
   skiq_write_rx_LO_freq(card, handle #1)
   skiq_write_rx_LO_freq(card, handle #2)
   skiq_start_rx_streaming_multi_immediate()
    while(receiving)
      skiq_receive(card, handle, rx_block, len)
   skiq_stop_rx_streaming_multi_immediate()
   skiq_exit()

.. note:: **\*** Different sample rates can be used per handle, but with the following caveats:

          1. The lower rate must be on a handle supporting decimation. (:code:`A1 or A2`)

          2. Rates must be < 153.6Msps

          3. The lower rate must be a factor of the higher rate

.. caution:: Phase coherent receive cannot be used in this scenario since decimation is active.


