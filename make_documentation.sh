#! /bin/bash
set -eo pipefail

abort()
{
    echo $? >&2

    echo >&2 '***************'
    echo >&2 '*** ABORTED ***'
    echo >&2 '***************
'
    echo "An error occurred. Exiting..." >&2
    exit 1
}

# set up bash to call 'abort' function whenever a non-zero exit code is encountered
# * https://stackoverflow.com/a/22224317
# * https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/
trap 'abort' 0

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

#
# To build documentation with the docker images, perform these commands once on a machine that has
# docker installed:
#
#    docker pull docker.epiq.rocks/epiq-sphinx-doc:1.7.1
#    docker pull docker.epiq.rocks/epiq-sphinx-doc:1.7.1-latex
#

HTML_DOCKER_IMAGE=docker.epiq.rocks/epiq-sphinx-doc:1.7.1
LATEX_DOCKER_IMAGE=docker.epiq.rocks/epiq-sphinx-doc:1.7.1-latex

function make-sphinx-doc()
{
    local USE_DOCKER=no
    local DOCKER_IMAGE=$1
    local TARGET=$2

    if hash docker 2>/dev/null; then
        if docker inspect "$DOCKER_IMAGE" >/dev/null 2>&1; then
            USE_DOCKER=yes
        fi
    fi

    if [ x"yes" = x"$USE_DOCKER" ]; then
        docker run --rm -u $(id -u):$(id -g) \
               -v "$SPHINX_SDK_DOCUMENTATION_DIR":/home/python/docs \
               "$DOCKER_IMAGE" make -C docs "$TARGET"
    elif hash sphinx-build 2>/dev/null; then
        make -C "$SPHINX_SDK_DOCUMENTATION_DIR" "$TARGET"
    else
        echo >&2
        echo -e "\tSphinx not installed or not found; not building SDK Documentation" >&2
        echo >&2
        echo -e "\tYou may need to follow the instructions in make_documentation.sh to create the required docker images" >&2
        echo >&2
        echo -e "\tThis script was not able to use '$DOCKER_IMAGE' successfully" >&2
        echo >&2
        exit 1
    fi
}

if [[ "$OS_TYPE" != "Msys" ]]; then
    SPHINX_SDK_DOCUMENTATION_DIR=$DIR
    echo -e "\tBuilding HTML SDK Documentation with Sphinx..." >&2
    make-sphinx-doc "$HTML_DOCKER_IMAGE" html
    res=$?
    if [ $res -ne 0 ]; then
        echo -e "\tFailed to build Sphinx HTML documentation (result $res)!" >&2
    else
        echo >&2
        echo -e "\tSphinx HTML documentation found in '$SPHINX_SDK_DOCUMENTATION_DIR/build/html'" >&2

        echo -e "\tBuilding PDF SDK Documenation with Sphinx..." >&2
        make-sphinx-doc "$LATEX_DOCKER_IMAGE" latexpdf
        res=$?
        if [ $res -ne 0 ]; then
            echo -e "\tFailed to build Sphinx PDF documentation (result $res)!"
        else
            echo >&2
            echo -e "\tSphinx PDF documentation found in '$SPHINX_SDK_DOCUMENTATION_DIR/build/latex'" >&2
            echo >&2
        fi
    fi
fi

# unregister the trap
trap : 0
