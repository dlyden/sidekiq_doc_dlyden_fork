#!/bin/bash

for ODT in Sidekiq_Software_Development_Manual_*.odt; do
    BASE=$(basename "$ODT" .odt)
    echo "generating $BASE.pdf" >&2
    libreoffice --headless --convert-to pdf:writer_pdf_Export $ODT
    ps2pdf $BASE.pdf
    mv $BASE.pdf.pdf $BASE.pdf
done
